# Forest GUMP

Forest GUMP is a tool for aggregating and interpreting Random Forests in a way that supports explanation, rapid evaluation, and code generation. A Random Forest is a widely used machine learning technique for classification and regression. Random Forests consists of a set of decision trees where each decision tree is learned independently in a random way. You can try out Forest GUMP here https://demo.forest-gump.k8s.ls-5.de/home.

## Building and Running Forest GUMP
### Prerequisites
- Java 11
- Docker and Docker Compose
### Building  Forest GUMP
- Clone this repository
- Download necessary libraries by executing the `download-libs.sh` script: `sh download-libs.sh`
- Download dime: https://scce.gitlab.io/dime/content/introduction/#download-the-installer
- Run dime:
	- Before running dime, make sure that Java 11 is installed (e.g. by checking the output of `java--version`)
	- A window asking you to choose a workspace will appear: Either use the default workspace or a a workspace of your choice and click on launch)
	- Click on "Import projects..." -> General -> Existing Projects into Workspace -> Choose the root directory of this repository
- Open `app.dad` file inside `dime-models/`
- Click on the "G-Button" on the top left (below "View") -> Wait for "Code generation successful" window -> Click on "OK"
- Inside the terminal:
	- Run the following command `sh copy-resources.sh`
	- Go into the `target` directory: `cd target/`
	- Run the following command to build and run Forest GUMP: `docker compose up --build`
	- Open http://127.0.0.1:8080/home to use Forest GUMP
