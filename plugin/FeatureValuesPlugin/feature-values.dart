import 'package:angular/angular.dart';
import 'dart:async';

@Component(
  selector: 'FeatureValues',
  templateUrl: 'feature-values.html',
  styleUrls: ['feature-values.css'],
  directives: [coreDirectives]
)
class FeatureValues {

  @Input() String method = '';
  @Input() List<String> labels = List();
  @Input() List<String> values = List();
  
  final resetValuesSC = new StreamController<Map<String, dynamic>>();
  @Output() Stream<Map<String,dynamic>> get resetValues => resetValuesSC.stream;
  
  dynamic map = {
    'trees': 'ADD Forest',
    'vector': 'Vector ADD',
    'majorityvote': 'Majority Vote',
    'modelexplanation': 'Model Explanation'
  };
  
  void resetSelectedValues() {
    resetValuesSC.add({
    	"resetValues": true
    });
  }
  
  String get text {
    var mergedLabels = List<String>();
    for (var i = 0; i < labels.length; i++) {
      mergedLabels.add(labels[i] + ': ' + values[i]);
    }
    return mergedLabels.join(', ');
  }
  
  String getSelectedMethod(String text) {
    if (text.startsWith('class')) {
      return 'Class Characterization: ' + text.split(' ')[1];
    } else {
	  return map[text];
    }
  }
}
