import 'package:angular/angular.dart';
import 'dart:js';
import 'dart:html';

@Component(
  selector: 'SvgDownload',
  templateUrl: 'svg-download.html',
  styleUrls: ['svg-download.css'],
  directives: [coreDirectives]
)
class SvgDownload {

  @Input() String label = '';
  @Input() String selector = '';
  @Input() String filename = '';
  @Input() int numberOfNodes = 0;
  @Input() int maxNumberOfNodes = 0;
  
  void download() {
    var el = document.querySelector(selector);
    if (el != null) {
      context.callMethod('downloadAddSvg', [el, filename]);
    }
  }
  
  bool get disabled {
    return numberOfNodes > maxNumberOfNodes;
  }
}
