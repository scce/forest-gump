import 'package:angular/angular.dart';
import 'package:angular/core.dart';
import 'package:app/src/models/Selectives.dart';
import 'dart:async';
import 'dart:html';
import '../../data/Data.dart' as data;

@Component(
    selector: 'SelectADDListComponent',
    templateUrl: 'select_add_list.html',
    styleUrls: const ["select_add_list.css"],
    directives: [coreDirectives]
)
class SelectADDListComponent {

  @Input() DIMEList<data.ADDInformation> history;

  final _selected = new StreamController<Map<String, DIMEList<data.ADDInformation>>>();
  @Output('selectAdds') Stream<Map<String,DIMEList<data.ADDInformation>>> get selected => _selected.stream;

  var selectedADDs = List<data.ADDInformation>();

  void submit() {
    var list = new DIMEList<data.ADDInformation>();
    list.addAll(selectedADDs);

    _selected.add({
      'selectedAdds': list
    });
    
    close();
  }

  void selectAdd(data.ADDInformation add) {
    if (isSelected(add)) {
      selectedADDs.remove(add);
    } else {
      selectedADDs.add(add);
    }
  }
  
  void close() {
  	document.querySelector('button[class=close]').click();
  }

  bool isSelected(data.ADDInformation add) {
    return selectedADDs.contains(add);
  }

  bool canSubmit() {
    return this.selectedADDs.length == 2;
  }
  
  List<data.ADDInformation> get filteredHistory {
    return history.where((i) => i.serializedADD != null).toList();
  }
}
