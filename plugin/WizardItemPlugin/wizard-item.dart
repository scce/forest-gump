import 'package:angular/angular.dart';

@Component(
  selector: 'WizardItem',
  templateUrl: 'wizard-item.html',
  styleUrls: ['wizard-item.css'],
  directives: [coreDirectives]
)
class WizardItem {

  @Input() String number = '';
  @Input() String title = '';
  @Input() String classes = '';
  @Input() bool show = false;
  
  String get cssClasses {
    return 'wizard-item panel panel-default ' + classes;
  }
}
