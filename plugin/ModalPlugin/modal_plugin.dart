import 'package:angular/angular.dart';
import 'package:angular/core.dart';
import 'package:app/src/models/Selectives.dart';
import 'dart:async';

@Component(
    selector: 'ModalComponent',
    templateUrl: 'modal_plugin.html',
    styleUrls: const ["modal_plugin.css"],
    directives: [coreDirectives]
)
class ModalComponent implements OnInit {

  @Input() String errorMessage;

  final _features = new StreamController<Map<String, dynamic>>();
  @Output() Stream<Map<String,dynamic>> get features => _features.stream;

  var featureValues;

  @override
  ngOnInit() {
    print("init ModalComponent");
    print(errorMessage);
  }

  void submitFeatureValues() {
    print(featureValues);
    _features.add({
      'features': new DIMEList<double>()
    });
  }

}
