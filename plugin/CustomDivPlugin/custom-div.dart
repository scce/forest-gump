import 'package:angular/angular.dart';

@Component(
  selector: 'CustomDiv',
  templateUrl: 'custom-div.html',
  styleUrls: ['custom-div.css'],
  directives: [coreDirectives]
)
class CustomDiv {

  @Input() String classes = '';
  @Input() String styles = '';
}
