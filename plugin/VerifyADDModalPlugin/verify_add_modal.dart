import 'package:angular/angular.dart';
import 'package:angular/core.dart';
import 'package:app/src/models/Selectives.dart';
import 'package:angular_forms/angular_forms.dart';
import 'dart:async';
import 'dart:html';
import '../../data/Data.dart' as data;

@Component(
    selector: 'VerifyADDModalComponent',
    templateUrl: 'verify_add_modal.html',
    styleUrls: const ["verify_add_modal.css"],
    directives: [coreDirectives, formDirectives]
)
class VerifyADDModalComponent implements OnInit {

  @Input() DIMEList<data.ADDInformation> history;
  @Input() DIMEList<data.StringWrapper> classLabels;
  @Input() List<String> attributeNames;
  
  final _verify = new StreamController<Map<String, dynamic>>();
  @Output('verify') Stream<Map<String,dynamic>> get verify => _verify.stream;
  
  var selectedClassLabels = List<String>();
  
  var preconditions = List<String>();
    
  var addForm = FormBuilder.controlGroup({
    'attribute': Control<String>('', Validators.required), 
    'operator': Control<String>('=', Validators.required),
    'value': Control<String>('', Validators.required)
  });
  
  @override
  void ngOnInit() {
    resetAddForm();
  }
  
  void resetAddForm() {
  	addForm.updateValue({
  	  'attribute': attributeNames[0],
  	  'operator': '==',
  	  'value': ''
  	});
  }
  
  void selectClassLabel(data.StringWrapper classLabel) {
  	if (isSelectedClassLabel(classLabel)) {
  	  selectedClassLabels.remove(classLabel.value);
  	} else {
  	  selectedClassLabels.add(classLabel.value);
  	}
  }
  
  bool isSelectedClassLabel(data.StringWrapper classLabel) {
  	return selectedClassLabels.contains(classLabel.value);
  } 
  
  void addPrecondition() {
    var precondition = '${this.addForm.value['attribute']} ${this.addForm.value['operator']} ${this.addForm.value['value']}';
    if (!preconditions.contains(precondition)) {
      preconditions.add(precondition);
    } 
    resetAddForm();
  }
  
  void removePrecondition(int index) {
    preconditions.removeAt(index);
  }

  void submit() {    
    Map<String, dynamic> output = Map();
    output['classLabels'] = new DIMEList<String>();
    output['classLabels'].addAll(selectedClassLabels);    
    output['preconditions'] = new DIMEList<String>();
    output['preconditions'].addAll(preconditions);
    output['information'] = history[0];
    
    _verify.add(output);
    close();
  }
  
  void close() {
    document.querySelector('button[class=close]').click();
  }

  bool canSubmit() {
    return selectedClassLabels.length > 0 && preconditions.length > 0;
  }
  
  data.ADDInformation get latestInformation {
  	return history[0];
  }
  
  bool get canUseVerification {
    return history.length > 0 && history[0].serializedADD != null;
  }
}
