import "dime-models/app.data" as data

dartPlugin ListFieldPlugin {
	styles ["plugin/ListFieldPlugin/list_field.css"]
	scripts ["plugin/ListFieldPlugin/list_field.dart"]
	template "plugin/ListFieldPlugin/list_field.html"
	
	component ListField {
		primitiveParameter "featureLabels" : text[] sync
		primitiveParameter "featureTypes" : text[] sync
		
		output features {
			primitiveParameter "features" : text[]
		}
		
	}
}

dartPlugin ModalPlugin {
	styles ["plugin/ModalPlugin/modal_plugin.css"]
	scripts ["plugin/ModalPlugin/modal_plugin.dart"]
	template "plugin/ModalPlugin/modal_plugin.html"
	
	component ModalComponent {
		primitiveParameter "errorMessage" : text sync
		
		output features {
			primitiveParameter "features" : real[]
		}
	}
	
}

dartPlugin SelectADDListPlugin {
	styles ["plugin/SelectADDList/select_add_list.css"]
    scripts ["plugin/SelectADDList/select_add_list.dart"]
    template "plugin/SelectADDList/select_add_list.html"
    
    component SelectADDListComponent {
		complexParameter "history" : data.ADDInformation[]
		
		output selectAdds {
			complexParameter "selectedAdds" : data.ADDInformation[]
		}
	}
}

dartPlugin VerifyADDModalPlugin {
	styles ["plugin/VerifyADDModalPlugin/verify_add_modal.css"]
    scripts ["plugin/VerifyADDModalPlugin/verify_add_modal.dart"]
    template "plugin/VerifyADDModalPlugin/verify_add_modal.html"
    
    component VerifyADDModalComponent {
		complexParameter "history" : data.ADDInformation[]
		complexParameter "classLabels" : data.StringWrapper[]
		primitiveParameter "attributeNames": text[] sync
		
		output verify {
			complexParameter "information" : data.ADDInformation
			primitiveParameter "classLabels" : text[]
			primitiveParameter "preconditions" : text[]
		}
	}
}


dartPlugin CustomDivPlugin {
	styles ["plugin/CustomDivPlugin/custom-div.css"]
    scripts ["plugin/CustomDivPlugin/custom-div.dart"]
    template "plugin/CustomDivPlugin/custom-div.html"
    
    placeholder [content]
    
    component CustomDiv {
        primitiveParameter "classes": text sync
        primitiveParameter "styles": text sync
    }
}

dartPlugin SkaffoldPlugin {
    scripts ["plugin/SkaffoldPlugin/skaffold.dart"]
    template "plugin/SkaffoldPlugin/skaffold.html"
    
    placeholder [sidebar,main]
    
    component Skaffold {
    }
}

dartPlugin WizardItemPlugin {
	styles ["plugin/WizardItemPlugin/wizard-item.css"]
    scripts ["plugin/WizardItemPlugin/wizard-item.dart"]
    template "plugin/WizardItemPlugin/wizard-item.html"
    
    placeholder [content]
        
    component WizardItem {
        primitiveParameter "number": text
        primitiveParameter "title": text
        primitiveParameter "classes": text
        primitiveParameter "show": boolean
    }
}

dartPlugin SeparatorPlugin {
	styles ["plugin/SeparatorPlugin/separator.css"]
    scripts ["plugin/SeparatorPlugin/separator.dart"]
    template "plugin/SeparatorPlugin/separator.html"
            
    component Separator {
    }
}

dartPlugin HistoryPanelPlugin {
	styles ["plugin/HistoryPanelPlugin/history-panel.css"]
    scripts ["plugin/HistoryPanelPlugin/history-panel.dart"]
    template "plugin/HistoryPanelPlugin/history-panel.html"
    
    placeholder [content]
            
    component HistoryPanel {
    }
}

dartPlugin FeatureValuesPlugin {
	styles ["plugin/FeatureValuesPlugin/feature-values.css"]
    scripts ["plugin/FeatureValuesPlugin/feature-values.dart"]
    template "plugin/FeatureValuesPlugin/feature-values.html"
                
    component FeatureValues {
        primitiveParameter "method" : text
        primitiveParameter "labels" : text[]
    	primitiveParameter "values" : text[]
    	
    	output resetValues {
			primitiveParameter "resetValues" : boolean
		}
    }
}

dartPlugin SvgDownloadPlugin {
	styles ["plugin/SvgDownloadPlugin/svg-download.css"]
    scripts ["plugin/SvgDownloadPlugin/svg-download.dart"]
    template "plugin/SvgDownloadPlugin/svg-download.html"
                
    component SvgDownload {
        primitiveParameter "label" : text
        primitiveParameter "filename" : text
        primitiveParameter "selector" : text
        primitiveParameter "numberOfNodes": integer
		primitiveParameter "maxNumberOfNodes": integer
    }
}

dartPlugin AutoLoginPlugin {
    scripts ["plugin/AutoLoginPlugin/auto-login.dart"]
    template "plugin/AutoLoginPlugin/auto-login.html"
                
    component AutoLogin {
    	primitiveParameter "username" : text
    	primitiveParameter "password" : text
    }
}

dartPlugin GraphOverlayPlugin {
	styles ["plugin/GraphOverlayPlugin/graph-overlay.css"]
    scripts ["plugin/GraphOverlayPlugin/graph-overlay.dart"]
    template "plugin/GraphOverlayPlugin/graph-overlay.html"
                
    component GraphOverlay {
    	primitiveParameter "numberOfNodes" : integer
    	primitiveParameter "pathLength" : integer
    }
}

plugin ToggleSwitchPlugin {
	styles ["plugin/ToggleSwitchPlugin/toggle_switch.css"]
	scripts ["plugin/ToggleSwitchPlugin/index.js", "plugin/ToggleSwitchPlugin/d3.js", "plugin/ToggleSwitchPlugin/d3-graphviz.js", "plugin/ToggleSwitchPlugin/toggle_switch.js"]
	template "plugin/ToggleSwitchPlugin/toggle_switch.html"
	
	placeholder [content,info]
	
	
	function renderSerializedDDs recalled {
		primitiveParameter "serializedDDs" : text
		primitiveParameter "numberOfNodes": integer
		primitiveParameter "maxNumberOfNodes": integer
	}
	
}
