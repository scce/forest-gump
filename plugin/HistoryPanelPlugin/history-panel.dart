import 'package:angular/angular.dart';

@Component(
  selector: 'HistoryPanel',
  templateUrl: 'history-panel.html',
  styleUrls: ['history-panel.css'],
  directives: [coreDirectives]
)
class HistoryPanel {

	bool collapsed = true;
}
