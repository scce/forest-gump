function downloadAddSvg(svg, filename) {
	const panningRectEl = svg.querySelector('g');
    const originalWidth = panningRectEl.getAttributeNS(null, 'width');
    const originalHeight = panningRectEl.getAttributeNS(null, 'height');

    panningRectEl.setAttributeNS(null, 'width', '0');
    panningRectEl.setAttributeNS(null, 'height', '0');

    // copy svg to prevent the svg being clipped due to the window size
    const svgCopy = svg.cloneNode(true);
    const g = svg.querySelector('g');
    const transform = g.getAttribute('transform');
    g.removeAttribute('transform');

    // set proper xml attributes for downloadable file
    svgCopy.setAttribute('version', '1.1');
    svgCopy.setAttribute('xmlns', 'http://www.w3.org/2000/svg');

    const dimension = g.getBoundingClientRect();

    svgCopy.setAttributeNS(null,'width', '' + dimension.width);
    svgCopy.setAttributeNS(null,'height', '' + dimension.height);

    const h = svg.getAttribute('viewBox').split(/\s+|,/)[3];
    svgCopy.querySelector('g').setAttribute('transform', 'translate(0, ' + h + ') scale(1)');

    const svgSaver = new SvgSaver();
    svgSaver.asSvg(svgCopy, filename + '.svg');

    g.setAttribute('transform', transform);
    panningRectEl.setAttributeNS(null, 'width', '' + originalWidth);
    panningRectEl.setAttributeNS(null, 'height', '' + originalHeight);
}