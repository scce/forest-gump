import 'package:angular/angular.dart';

@Component(
  selector: 'GraphOverlay',
  templateUrl: 'graph-overlay.html',
  styleUrls: ['graph-overlay.css'],
  directives: [coreDirectives]
)
class GraphOverlay {

  @Input() int numberOfNodes = 0;
  @Input() int pathLength = 0;
}
