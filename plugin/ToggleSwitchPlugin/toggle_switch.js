let g;
let svg;
let margin = 20;
let zoomScaleExtent = [0.5, 10];
let graphviz;

function resizeSVG() {
	var width = window.innerWidth*0.7;
	var height = window.innerHeight*0.7;
	d3.select("#graph").selectWithoutDataPropagation("svg")
		.transition()
		.duration(700)
}

d3.select(window).on("resize", resizeSVG);

function renderSerializedDDs(serializedDD, numberOfNodes, maxNumberOfNodes) {
	if (numberOfNodes <= maxNumberOfNodes) {
	  const graphContainer = d3.select("#graph");
	  graphviz = graphContainer.graphviz(false)
		.zoomScaleExtent(zoomScaleExtent)
		.on('renderEnd', () => {
			svg = d3.select("#graph svg");
			g = d3.select("#graphq svg g");
		})
		.renderDot(serializedDD);
	}
}

function transition(zoomLevel) {
	svg.transition()
		.duration(700)
		.call(graphviz.zoomBehavior().scaleBy, zoomLevel);
}

function zoomIn() {
	transition(1.2);
}

function zoomOut() {
	transition(0.8);
}

function zoomReset() {
	graphviz.resetZoom();
}
