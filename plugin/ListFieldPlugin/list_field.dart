import 'package:angular/angular.dart';
import 'package:angular/core.dart';
import 'package:app/src/models/Selectives.dart';
import 'dart:async';

@Component(
    selector: 'ListField',
    templateUrl: 'list_field.html',
    styleUrls: const ["list_field.css"],
    directives: [coreDirectives]
)
class ListField implements OnInit {

  @Input() List<String> featureLabels;
  @Input() List<String> featureTypes;

  final _features = new StreamController<Map<String, dynamic>>();
  @Output() Stream<Map<String,dynamic>> get features => _features.stream;

  var featureValues;

  @override
  ngOnInit() {
    print("init");
    print(featureLabels);
    print(featureTypes);
    featureValues = new DIMEList<String>();
    for(int i = 0; i < featureLabels.length; i++) {
      featureValues.add("");
    }
  }

  void addFeatureValue(dynamic evt, int index) {
    featureValues[index] = evt.target.value;
  }

  void submitFeatureValues(dynamic e) {
    e.preventDefault();
    print(featureValues);
    _features.add({
      'features': featureValues
    });
  }

  String getType(int index) {
    if(featureTypes[index] == "NUMERIC") {
      return "number";
    } else {
      return "text";
    }
  }

}
