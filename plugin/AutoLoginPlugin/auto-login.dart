import 'dart:html';
import 'package:angular/angular.dart';
import 'package:app/src/services/AuthService.dart';

@Component(
  selector: 'AutoLogin',
  templateUrl: 'auto-login.html',
  directives: [coreDirectives]
)
class AutoLogin implements OnInit {

  @Input() String username = '';
  @Input() String password = '';
  
  AuthService authService;
  
  AutoLogin(this.authService) {}

  void ngOnInit() async {
    await this.authService.auth(username, password);
    window.location.reload();
  }
}
