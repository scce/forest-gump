#!/bin/bash

cp ./resources/development-gcc.Dockerfile ./target/development-gcc.Dockerfile
cp ./resources/docker-compose.yml ./target/docker-compose.yml
cp ./libs/graphvizlib.wasm ./target/webapp/web/js/postcombined/graphvizlib.wasm
