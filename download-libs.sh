#!/bin/bash

mkdir -p ./libs
curl -o ./libs/graphvizlib.wasm https://cdn.jsdelivr.net/npm/@hpcc-js/wasm@1.4.1/dist/graphvizlib.wasm
curl -o ./libs/svgsaver.js https://cdn.jsdelivr.net/npm/svgsaver@0.9.0/browser.js
