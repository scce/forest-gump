package info.scce.dime.app.forest;

import com.google.common.collect.BiMap;
import info.scce.addlib.infrandforest.dd.labelmultiset.LabelMultiset;

public class MultisetPrettifier implements LeafLabelPrettifier<LabelMultiset> {

    @Override
    public String prettifyLabel(LabelMultiset labelMultiset, BiMap<Integer, String> labelIdx) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < labelIdx.size(); i++) {
            int labelCount = labelMultiset.count(i);
            String label = labelIdx.get(i);
            sb.append(label).append("=").append(labelCount);
            if (i != labelIdx.size() - 1) { sb.append(";"); }
        }
        return sb.toString();
    }
}
