package info.scce.dime.app.forest.generator;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.app.ARFFAttribute;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.infrandforest.dd.PredicateNaming;

public abstract class PredicatesGenerator {

    protected final List<XDD<?>> xdds;
    protected final PredicateNaming predNaming;
    protected final List<ARFFAttribute> arffAttributes;
    protected final Set<String> foundLabels;
    protected final List<String> orderedLabels;

    public PredicatesGenerator(List<XDD<?>> xdds, PredicateNaming predNaming, List<ARFFAttribute> arffAttributes) {
        this.xdds = xdds;
        this.predNaming = predNaming;
        this.arffAttributes = arffAttributes;
        this.foundLabels = getLabels();
        this.orderedLabels = new ArrayList<>(foundLabels);
    }

    public abstract List<String> getFileNames();

    public abstract List<String> getFileContents();

    private Set<String> getLabels() {
        Set<String> labels = new HashSet<>();
        xdds.forEach(dd -> findLabels(dd, labels));
        return labels;
    }

    private void findLabels(XDD<?> xdd, Set<String> labels) {
        if (!xdd.isConstant()) {
            String ddLabel = xdd.readName();
            labels.add(ddLabel);
            findLabels(xdd.t(), labels);
            findLabels(xdd.e(), labels);
        }
    }

}
