package info.scce.dime.app.forest;

import com.google.common.collect.BiMap;
import info.scce.addlib.infrandforest.dd.labelvector.LabelVector;

public class LabelVectorPrettifier implements LeafLabelPrettifier<LabelVector> {

    @Override
    public String prettifyLabel(LabelVector labelVector, BiMap<Integer, String> labelIdx) {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (int i = 0; i < labelVector.size(); i++) {
            String label = labelIdx.get(labelVector.get(i));
            sb.append(label);
            if (i != labelVector.size() - 1) { sb.append(","); }
        }
        sb.append("]");
        return sb.toString();
    }
}
