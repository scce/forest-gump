package info.scce.dime.app.forest.generator;

import java.util.ArrayList;
import java.util.List;

import de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.app.ARFFAttribute;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.infrandforest.dd.PredicateNaming;

public class PredicatesGeneratorEmpty extends PredicatesGenerator {

    public PredicatesGeneratorEmpty(List<XDD<?>> xdds, PredicateNaming predNaming, List<ARFFAttribute> arffAttributes) {
        super(xdds, predNaming, arffAttributes);
    }

    public PredicatesGeneratorEmpty() {
        super(new ArrayList<>(), null, null);
    }

    @Override
    public List<String> getFileNames() {
        return new ArrayList<>();
    }

    @Override
    public List<String> getFileContents() {
        return new ArrayList<>();
    }

}
