package info.scce.dime.app.forest;

import com.google.common.collect.BiMap;

public interface LeafLabelPrettifier<E> {

    public String prettifyLabel(E label, BiMap<Integer, String> labelIdx);

}
