package info.scce.dime.app.forest.generator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.app.ARFFAttribute;
import de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.app.ARFFDatatype;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.infrandforest.assignment.NominalPredicate;
import info.scce.addlib.infrandforest.assignment.Predicate;
import info.scce.addlib.infrandforest.assignment.RealPredicate;
import info.scce.addlib.infrandforest.dd.PredicateNaming;

public class PredicatesGeneratorJava extends PredicatesGenerator {

    public PredicatesGeneratorJava(List<XDD<?>> xdds, PredicateNaming predNaming, List<ARFFAttribute> arffAttributes) {
        super(xdds, predNaming, arffAttributes);
    }

    @Override
    public List<String> getFileNames() {
        return new ArrayList<>(Arrays.asList("Predicates.java", "PredicatesDefault.java"));
    }

    @Override
    public List<String> getFileContents() {
        return new ArrayList<>(Arrays.asList(generateJavaInterface(), generateJavaImplementation()));
    }

    private String generateJavaInterface() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("public interface Predicates {");
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append(System.lineSeparator());
        generateInterfaceMethods(stringBuilder);
        stringBuilder.append("}");
        return stringBuilder.toString();
    }

    private void generateInterfaceMethods(StringBuilder stringBuilder) {
        for (String ddLabel : foundLabels) {
            stringBuilder.append("\tboolean ").append(ddLabel).append("();");
            stringBuilder.append(System.lineSeparator());
            stringBuilder.append(System.lineSeparator());
        }
    }

    private String generateJavaImplementation() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("public class PredicatesDefault implements Predicates {");
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append(System.lineSeparator());

        appendJavaClassAttributes(stringBuilder);
        stringBuilder.append(System.lineSeparator());
        appendJavaConstructor(stringBuilder);
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append(System.lineSeparator());

        for (String label : foundLabels) {
            Predicate predicate = predNaming.predicate(label);
            String argument = predicate.argument();
            if (predicate instanceof NominalPredicate) {
                String referenceValue = ((NominalPredicate) predicate).referenceValue();
                stringBuilder.append("\tpublic boolean ").append(label).append("() {");
                stringBuilder.append(System.lineSeparator());
                stringBuilder.append("\t\t return ")
                             .append(argument)
                             .append(".equals(\"")
                             .append(referenceValue)
                             .append("\");");
                stringBuilder.append(System.lineSeparator());
                stringBuilder.append("\t}");
                stringBuilder.append(System.lineSeparator());
            } else if (predicate instanceof RealPredicate) {
                double threshold = ((RealPredicate) predicate).threshold();
                stringBuilder.append("\tpublic boolean ").append(label).append("() {");
                stringBuilder.append(System.lineSeparator());
                stringBuilder.append("\t\t return ").append(argument).append(" < ").append(threshold).append(";");
                stringBuilder.append(System.lineSeparator());
                stringBuilder.append("\t}");
                stringBuilder.append(System.lineSeparator());
            }
            stringBuilder.append(System.lineSeparator());
        }
        stringBuilder.append("}");
        return stringBuilder.toString();
    }

    private void appendJavaClassAttributes(StringBuilder stringBuilder) {
        for (ARFFAttribute attribute : arffAttributes) {
            stringBuilder.append("\tprivate ")
                         .append(arffDatatypeToJavaType(attribute.getarffDataype()))
                         .append(" ")
                         .append(attribute.getname())
                         .append(";");
            stringBuilder.append(System.lineSeparator());
        }
    }

    private void appendJavaConstructor(StringBuilder stringBuilder) {
        stringBuilder.append("\tpublic PredicatesDefault(").append(generateJavaArguments()).append("){");
        stringBuilder.append(System.lineSeparator());
        appendJavaConstructorBody(stringBuilder);
        stringBuilder.append("\t}");
    }

    private String generateJavaArguments() {
        Set<String> javaArguments = new HashSet<>();
        for (ARFFAttribute attribute : arffAttributes) {
            javaArguments.add(arffDatatypeToJavaType(attribute.getarffDataype()) + " " + attribute.getname());
        }
        return String.join(", ", javaArguments);
    }

    private String arffDatatypeToJavaType(ARFFDatatype datatype) {
        switch (datatype) {
            case NUMERIC:
                return "double";
            case NOMINAL:
                return "String";
            default:
                throw new IllegalArgumentException("Unsupported ARFFDatatype");
        }
    }

    private void appendJavaConstructorBody(StringBuilder stringBuilder) {
        for (ARFFAttribute attribute : arffAttributes) {
            stringBuilder.append("\t\tthis.")
                         .append(attribute.getname())
                         .append(" = ")
                         .append(attribute.getname())
                         .append(";");
            stringBuilder.append(System.lineSeparator());
        }
    }

}
