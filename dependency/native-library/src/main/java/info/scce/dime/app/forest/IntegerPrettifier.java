package info.scce.dime.app.forest;

import com.google.common.collect.BiMap;

public class IntegerPrettifier implements LeafLabelPrettifier<Integer> {

    @Override
    public String prettifyLabel(Integer label, BiMap<Integer, String> labelIdx) {
        return labelIdx.get(label);
    }
}
