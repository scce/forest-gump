package info.scce.dime.app.forest;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.common.base.Strings;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Range;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.dd.xdd.XDDManager;
import info.scce.addlib.infrandforest.assignment.NominalPredicate;
import info.scce.addlib.infrandforest.assignment.Predicate;
import info.scce.addlib.infrandforest.assignment.RealPredicate;
import info.scce.addlib.infrandforest.dd.PredicateNaming;

public class RedundantPredicateEliminationNew<E> {

    protected final XDDManager<E> ddManager;
    protected final PredicateNaming predNaming;

    public RedundantPredicateEliminationNew(XDDManager<E> ddManager, PredicateNaming predNaming) {
        this.ddManager = ddManager;
        this.predNaming = predNaming;
    }

    public XDD<E> apply(XDD<E> f) {

        /* Derive argument domains from diagram */
        Map<String, Range<Double>> realDomains = new HashMap<>();
        Map<String, Set<String>> nominalDomains = new HashMap<>();
        collectDomains(f, realDomains, nominalDomains);

        return eliminateRedundantPredicates(f, realDomains, nominalDomains);
    }

    private XDD<E> eliminateRedundantPredicates(XDD<E> f,
                                                Map<String, Range<Double>> realDomains,
                                                Map<String, Set<String>> nominalDomains) {

        /* No infeasible paths in constant function */
        if (f.isConstant()) { return f.withRef(); }

        Predicate p = predNaming.predicate(f.readName());
        if (p instanceof RealPredicate) {
            RealPredicate rp = (RealPredicate) p;
            String argument = rp.argument();

            /* Range of possible values on this path */
            Range<Double> range = realDomains.get(argument);
            assert !range.isEmpty();

            /* Express predicate in ranges */
            RealPredicate.Comparator cmp = rp.comparator();
            Range tRange, eRange;
            if (cmp == RealPredicate.Comparator.LT) {
                tRange = Range.lessThan(rp.threshold());
                eRange = Range.atLeast(rp.threshold());
            } else if (cmp == RealPredicate.Comparator.LEQ) {
                tRange = Range.atMost(rp.threshold());
                eRange = Range.greaterThan(rp.threshold());
            } else if (cmp == RealPredicate.Comparator.GT) {
                tRange = Range.greaterThan(rp.threshold());
                eRange = Range.atMost(rp.threshold());
            } else if (cmp == RealPredicate.Comparator.GEQ) {
                tRange = Range.atLeast(rp.threshold());
                eRange = Range.lessThan(rp.threshold());
            } else {
                throw new RuntimeException("unexpected comparator " + cmp);
            }

            if (tRange.encloses(range)) {

                /* Predicate must always hold */
                return eliminateRedundantPredicates(f.t(), realDomains, nominalDomains);

            } else if (eRange.encloses(range)) {

                /* Predicate never holds */
                return eliminateRedundantPredicates(f.e(), realDomains, nominalDomains);

            } else {

                /* Predicate may or may not hold */
                assert !range.intersection(tRange).isEmpty() && !range.intersection(eRange).isEmpty();

                /* Recursive call for case in which predicate holds */
                realDomains.put(argument, range.intersection(tRange));
                XDD<E> t = eliminateRedundantPredicates(f.t(), realDomains, nominalDomains);

                /* Recursive call for case in which predicate does not hold */
                realDomains.put(argument, range.intersection(eRange));
                XDD<E> e = eliminateRedundantPredicates(f.e(), realDomains, nominalDomains);

                /* Restore argument */
                realDomains.put(argument, range);

                /* Construct result diagram */
                int i = f.readIndex();
                XDD<E> result = ddManager.ithVar(i, t, e);
                t.recursiveDeref();
                e.recursiveDeref();
                return result;

            }
        } else if (p instanceof NominalPredicate) {
            NominalPredicate np = (NominalPredicate) p;
            String argument = np.argument();

            /* Set of possible values on this path */
            Set<String> domain = nominalDomains.get(argument);
            assert !domain.isEmpty();

            if (domain.size() == 1 && domain.contains(np.referenceValue())) {

                /* Predicate always holds */
                return eliminateRedundantPredicates(f.t(), realDomains, nominalDomains);

            } else if (!domain.contains(np.referenceValue())) {

                /* Predicate never holds */
                return eliminateRedundantPredicates(f.e(), realDomains, nominalDomains);

            } else {

                /* Predicate may or may not hold */
                NominalPredicate.Comparator cmp = np.comparator();

                /* Recursive call for case in which predicate holds */
                Set<String> tDomain = Collections.singleton(np.referenceValue());
                nominalDomains.put(argument, tDomain);
                XDD<E> t = eliminateRedundantPredicates(f.t(), realDomains, nominalDomains);

                /* Recursive call for case in which predicate does not holds */
                Set<String> eDomain = domain;
                assert eDomain.contains(np.referenceValue());
                eDomain.remove(np.referenceValue());
                nominalDomains.put(argument, eDomain);
                XDD<E> e = eliminateRedundantPredicates(f.e(), realDomains, nominalDomains);

                /* Restore argument */
                assert !domain.contains(np.referenceValue());
                domain.add(np.referenceValue());
                assert nominalDomains.get(argument) == domain && domain.contains(np.referenceValue());

                /* Swap t and e if neagted predicate */
                if (cmp == NominalPredicate.Comparator.NEQ) {
                    XDD<E> tmp = t;
                    e = t;
                    t = tmp;
                } else {
                    assert cmp == NominalPredicate.Comparator.EQ;
                }

                /* Construct result diagram */
                XDD<E> result = ddManager.ithVar(f.readIndex(), t, e);
                t.recursiveDeref();
                e.recursiveDeref();
                return result;

            }
        } else {
            throw new RuntimeException("unexpected predicate type " + p.getClass().getSimpleName());
        }
    }

    private void collectDomains(XDD<E> dd,
                                Map<String, Range<Double>> realDomains,
                                Map<String, Set<String>> nominalDomains) {

        /* Collect domains as appearences */
        collectDomainsRecursively(dd, realDomains, nominalDomains);

        /* Add value for all non-referenced nominal values */
        for (Set<String> domain : nominalDomains.values()) {

            /* Preferably 'other' */
            String otherValue = "other";

            /* If that is taken, we use 'ooooo...other' to avoid the collision */
            if (domain.contains(otherValue)) {
                int maxValueLength = 0;
                for (String value : domain) { maxValueLength = Math.max(maxValueLength, value.length()); }
                otherValue = Strings.repeat(otherValue.substring(0, 1), maxValueLength) + otherValue;
            }

            domain.add(otherValue);
        }
    }

    private void collectDomainsRecursively(XDD<E> dd,
                                           Map<String, Range<Double>> realDomains,
                                           Map<String, Set<String>> nominalDomains) {
        if (dd.isConstant()) { return; }
        Predicate p = this.predNaming.predicate(dd.readName());
        if (p instanceof RealPredicate) {
            RealPredicate rp = (RealPredicate) p;
            String argument = rp.argument();

            /* Ensure argument in real domain */
            if (!realDomains.containsKey(argument)) { realDomains.put(argument, Range.all()); }

        } else if (p instanceof NominalPredicate) {
            NominalPredicate np = (NominalPredicate) p;
            String argument = np.argument();

            /* Ensure argument in nominal domain and collect possible value */
            if (!nominalDomains.containsKey(argument)) { nominalDomains.put(argument, new HashSet<>()); }
            nominalDomains.get(argument).add(np.referenceValue());

        } else {
            throw new RuntimeException("unexpected predicate type " + p.getClass().getSimpleName());
        }

        /* Recur */
        collectDomainsRecursively(dd.t(), realDomains, nominalDomains);
        collectDomainsRecursively(dd.e(), realDomains, nominalDomains);
    }

    public String name(Predicate p) {
        return predNaming.name(p);
    }

}
