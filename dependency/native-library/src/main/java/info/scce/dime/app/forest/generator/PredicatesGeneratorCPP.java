package info.scce.dime.app.forest.generator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.app.ARFFAttribute;
import de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.app.ARFFDatatype;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.infrandforest.assignment.NominalPredicate;
import info.scce.addlib.infrandforest.assignment.Predicate;
import info.scce.addlib.infrandforest.assignment.RealPredicate;
import info.scce.addlib.infrandforest.dd.PredicateNaming;

public class PredicatesGeneratorCPP extends PredicatesGenerator {

    public PredicatesGeneratorCPP(List<XDD<?>> xdds, PredicateNaming predNaming, List<ARFFAttribute> arffAttributes) {
        super(xdds, predNaming, arffAttributes);
    }

    @Override
    public List<String> getFileNames() {
        return new ArrayList<>(Arrays.asList("Predicates.h", "PredicatesDefault.h"));
    }

    @Override
    public List<String> getFileContents() {
        return new ArrayList<>(Arrays.asList(generateInterface(), generateImplementation()));
    }

    private String generateInterface() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("class Predicates {");
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append(System.lineSeparator());

        stringBuilder.append("\tpublic:");
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append(System.lineSeparator());

        stringBuilder.append("\tPredicates() {}");
        stringBuilder.append(System.lineSeparator());
        appendVirtualFunctions(stringBuilder);

        stringBuilder.append(System.lineSeparator());
        stringBuilder.append("};");

        return stringBuilder.toString();
    }

    private void appendVirtualFunctions(StringBuilder stringBuilder) {
        for (String foundLabel : foundLabels) {
            stringBuilder.append("\tvirtual bool ").append(foundLabel).append("() {");
            stringBuilder.append(System.lineSeparator());
            stringBuilder.append("\t\treturn false;");
            stringBuilder.append(System.lineSeparator());
            stringBuilder.append("\t}");
            stringBuilder.append(System.lineSeparator());
            stringBuilder.append(System.lineSeparator());
        }
    }

    private String generateImplementation() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("#include <string>");
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append("class PredicatesDefault: public Predicates {");
        stringBuilder.append(System.lineSeparator());

        stringBuilder.append("\tpublic:");
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append(System.lineSeparator());

        stringBuilder.append("\tPredicatesDefault(").append(getConstructorArguments()).append("{}");
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append(System.lineSeparator());

        appendFunctionImplementations(stringBuilder);

        stringBuilder.append("\tprivate:");
        stringBuilder.append(System.lineSeparator());
        for (ARFFAttribute attribute : arffAttributes) {
            stringBuilder.append("\t\t")
                         .append(arffDatatypeToCPPType(attribute.getarffDataype()))
                         .append(" ")
                         .append(attribute.getname())
                         .append(";");
            stringBuilder.append(System.lineSeparator());
        }
        stringBuilder.append("};");
        return stringBuilder.toString();
    }

    private void appendFunctionImplementations(StringBuilder stringBuilder) {
        for (String ddLabel : foundLabels) {
            Predicate predicate = predNaming.predicate(ddLabel);
            String argument = predicate.argument();
            stringBuilder.append("\tbool ").append(ddLabel).append("() {");
            stringBuilder.append(System.lineSeparator());
            if (predicate instanceof NominalPredicate) {
                String referenceValue = ((NominalPredicate) predicate).referenceValue();
                stringBuilder.append("\t\treturn ").append(argument).append(" == \"").append(referenceValue).append("\";");
                stringBuilder.append(System.lineSeparator());
                stringBuilder.append(System.lineSeparator());
            } else if (predicate instanceof RealPredicate) {
                double threshold = ((RealPredicate) predicate).threshold();
                stringBuilder.append("\t\treturn ").append(argument).append(" < ").append(threshold).append(";");
            }
            stringBuilder.append(System.lineSeparator());
            stringBuilder.append("\t}");
            stringBuilder.append(System.lineSeparator());
        }
    }

    private String getConstructorArguments() {
        List<String> arguments = new ArrayList<>();
        for (ARFFAttribute attribute : arffAttributes) {
            arguments.add(arffDatatypeToCPPType(attribute.getarffDataype()) + " " + attribute.getname());
        }
        String joinedArguments = String.join(", ", arguments);

        List<String> initializations = new ArrayList<>();
        for (ARFFAttribute attribute : arffAttributes) {
            initializations.add(attribute.getname() + "(" + attribute.getname() + ")");
        }
        String joinedInitializations = String.join(", ", initializations);
        return joinedArguments + ") : " + joinedInitializations;
    }

    private String arffDatatypeToCPPType(ARFFDatatype datatype) {
        switch (datatype) {
            case NUMERIC:
                return "double";
            case NOMINAL:
                return "std::string";
            default:
                throw new IllegalArgumentException("Unsupported ARFFDatatype");
        }
    }

}
