package info.scce.dime.app.forest;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Range;
import de.ls5.dywa.generated.controller.dime__HYPHEN_MINUS__models.app.ADDInformationController;
import de.ls5.dywa.generated.controller.dime__HYPHEN_MINUS__models.app.DataSetController;
import de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.app.ADDInformation;
import de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.app.ADDType;
import de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.app.ARFFAttribute;
import de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.app.ARFFDatatype;
import de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.app.DataSet;
import de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.app.DataSetType;
import de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.app.GeneratorType;
import de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.app.VerifyADDInputData;
import info.scce.dime.util.DomainFileController;
import info.scce.dime.util.FileReference;
import info.scce.addlib.codegenerator.CPPGenerator;
import info.scce.addlib.codegenerator.CodeGenerator;
import info.scce.addlib.codegenerator.DotGenerator;
import info.scce.addlib.codegenerator.JavaGenerator;
import info.scce.addlib.codegenerator.PythonGenerator;
import info.scce.addlib.dd.LabelledRegularDD;
import info.scce.addlib.dd.RegularDD;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.dd.xdd.XDDManager;
import info.scce.addlib.dd.xdd.grouplikedd.MonoidDDManager;
import info.scce.addlib.infrandforest.assignment.NominalPredicate;
import info.scce.addlib.infrandforest.assignment.RealPredicate;
import info.scce.addlib.infrandforest.dd.ClassDDManager;
import info.scce.addlib.infrandforest.dd.DDEquality;
import info.scce.addlib.infrandforest.dd.PredicateNaming;
import info.scce.addlib.infrandforest.dd.elimination.RedundancyElimination;
import info.scce.addlib.infrandforest.dd.elimination.RedundancyEliminationPOE;
import info.scce.addlib.infrandforest.dd.labelmultiset.LabelMultiset;
import info.scce.addlib.infrandforest.dd.labelmultiset.LabelMultisetDDManager;
import info.scce.addlib.infrandforest.dd.labelvector.LabelVector;
import info.scce.addlib.infrandforest.dd.labelvector.LabelVectorDDManager;
import info.scce.addlib.serializer.DDProperty;
import info.scce.addlib.serializer.XDDSerializer;
import info.scce.dime.app.forest.generator.PredicatesGenerator;
import info.scce.dime.app.forest.generator.PredicatesGeneratorCPP;
import info.scce.dime.app.forest.generator.PredicatesGeneratorEmpty;
import info.scce.dime.app.forest.generator.PredicatesGeneratorJava;
import info.scce.dime.app.forest.generator.PredicatesGeneratorPython;

public class Native {

    private static DomainFileController dfc;
    private static ADDInformationController addInfoController;
    private static DataSetController dataSetController;

    public static ADDInformation generateEquivalenceADD(List<ADDInformation> adds) {
        System.out.println("adds size: " + adds.size());
        if (adds.size() != 2) {
            throw new IllegalArgumentException("Number of adds is " + adds.size() + " but must be 2");
        }

        System.out.println(adds.get(0).getserializedADD());
        System.out.println(adds.get(1).getserializedADD());

        ClassDDManager xddManager = new ClassDDManager();
        XDDSerializer<Integer> deserializer = new XDDSerializer<>();
        //TODO: Since the adds are created within different DDManagers,
        // it might not be possible to use enforce their initial variable order in a single DDManager.
        XDD<Integer> deserializedDD1 =
                deserializer.deserialize(xddManager, adds.get(0).getserializedADD(), DDProperty.VARNAMEANDVARINDEX);
        XDD<Integer> deserializedDD2 =
                deserializer.deserialize(xddManager, adds.get(1).getserializedADD(), DDProperty.VARNAME);
        PredicateNaming predicateNaming = new PredicateNaming();
        fillPredicateNaming(deserializedDD1, predicateNaming);
        fillPredicateNaming(deserializedDD2, predicateNaming);
        XDD<Integer> eqDD = DDEquality.equalityDD(deserializedDD1, deserializedDD2, 1, 0);

        RedundancyElimination<Integer> elimination = new RedundancyEliminationPOE<>(xddManager, predicateNaming);
        XDD<Integer> eqDDNoInfeasiblePaths = elimination.elimRedundancy(eqDD);
        eqDD.recursiveDeref();
        info.scce.addlib.infrandforest.util.DotGenerator<Integer> dotGenerator =
                new info.scce.addlib.infrandforest.util.DotGenerator<>(eqDDNoInfeasiblePaths,
                                                                       predicateNaming,
                                                                       (s, biMap) -> "" + s,
                                                                       null,
                                                                       "Equivalence");
        String generatedDot = dotGenerator.generateDot();
        String errorMessage = "";
        return createADDInfoVerification(adds.get(0), generatedDot, "Equivalence", errorMessage);
    }

    public static ADDInformation generateVerificationADD(VerifyADDInputData inputData) {
        ADDInformation addInfo = inputData.getinformation();
        List<String> postCondition = inputData.getclassLabels();
        System.out.println("postConditions: " + Arrays.toString(postCondition.toArray()));

        List<String> preconditions = inputData.getpreconditions();
        System.out.println("preconditions: " + Arrays.toString(preconditions.toArray()));

        List<String> classLabels = addInfo.getclassLabels();
        System.out.println("classLabels: " + Arrays.toString(classLabels.toArray()));

        System.out.println("addInfo labelIdx:" + addInfo.getlabelIdx());
        BiMap<Integer, String> idx2class = parseLabelIdx(addInfo.getlabelIdx());

        // check for invalid postconditions
        for (String cls : postCondition) {
            if (!classLabels.contains(cls)) {
                throw new IllegalArgumentException(cls + " is not a valid class");
            }
        }

        Set<Integer> classesIds = new HashSet<>();
        Map<String, String> class2color = new HashMap<>();
        for (Map.Entry<Integer, String> entry : idx2class.entrySet()) {
            int idx = entry.getKey();
            String cls = entry.getValue();
            if (postCondition.contains(cls)) {
                class2color.put(Integer.toString(idx), "darkseagreen");
            } else {
                class2color.put(Integer.toString(idx), "salmon");
            }
        }

        for (String cls : postCondition) {
            classesIds.add(idx2class.inverse().get(cls));
        }

        System.out.println("classesIds: " + classesIds);

        String serializedADD = addInfo.getserializedADD();
        System.out.println("serializedADD: " + serializedADD);
        ADDType ddType = addInfo.getaDDType();
        if (ddType == ADDType.MAJORITY_VOTE || ddType == ADDType.ModelExplanation) {
            ClassDDManager xddManager = new ClassDDManager();
            XDDSerializer<Integer> deserializer = new XDDSerializer<>();
            XDD<Integer> deserializedDD =
                    deserializer.deserialize(xddManager, serializedADD, DDProperty.VARNAMEANDVARINDEX);
            PredicateNaming predicateNaming = new PredicateNaming();
            fillPredicateNaming(deserializedDD, predicateNaming);
            RedundancyElimination<Integer> elimination = new RedundancyEliminationPOE<>(xddManager, predicateNaming);
            addPreconditions(elimination, preconditions);
            XDD<Integer> redundancyFreeDD = elimination.elimRedundancy(deserializedDD);
            info.scce.addlib.infrandforest.util.DotGenerator<Integer> dotGenerator =
                    new info.scce.addlib.infrandforest.util.DotGenerator<>(redundancyFreeDD,
                                                                           predicateNaming,
                                                                           new info.scce.addlib.infrandforest.util.IntegerPrettifier(),
                                                                           idx2class,
                                                                           "Verification");
            dotGenerator.setClass2color(class2color);
            boolean satisfiesProperty = satisfiesProperty(redundancyFreeDD, classesIds);
            if (satisfiesProperty) {
                dotGenerator.setRootColor("darkseagreen");
            } else {
                dotGenerator.setRootColor("salmon");
            }
            String generatedDot = dotGenerator.generateDot();
            String errorMessage = "";
            return createADDInfoVerification(addInfo, generatedDot, "Verification", errorMessage);
        } else {
            throw new IllegalArgumentException("ddType " + ddType + " not supported by verification");
        }
    }

    private static boolean satisfiesProperty(XDD<Integer> dd, Set<Integer> classesIds) {
        Map<XDD<Integer>, Boolean> cache = new HashMap<>();
        return satisfiesProperty(dd, classesIds, cache);
    }

    private static boolean satisfiesProperty(XDD<Integer> dd, Set<Integer> classesIds, Map<XDD<Integer>, Boolean> cache) {
        Boolean cachedResult = cache.get(dd);
        if (cachedResult == null) {
            if (dd.isConstant()) {
                cachedResult = classesIds.contains(dd.v());
            } else {
                cachedResult = satisfiesProperty(dd.t(), classesIds, cache);
                if (cachedResult) {
                    cachedResult = satisfiesProperty(dd.e(), classesIds, cache);
                }
            }
            cache.put(dd, cachedResult);
        }
        return cachedResult;
    }

    private static BiMap<Integer, String> parseLabelIdx(String labelIdx) {
        BiMap<Integer, String> classIdx2Class = HashBiMap.create();
        String[] split = labelIdx.split(",");
        for (String entry : split) {
            String[] splitEntry = entry.split("#");
            int idx = Integer.parseInt(splitEntry[0]);
            String cls = splitEntry[1];
            classIdx2Class.put(idx, cls);
        }
        return classIdx2Class;
    }

    private static void fillPredicateNaming(XDD<?> dd, PredicateNaming predicateNaming) {
        fillPredicateNaming(dd, predicateNaming, new HashSet<>());
    }

    private static void fillPredicateNaming(XDD<?> dd, PredicateNaming predicateNaming, Set<XDD<?>> seenDDs) {
        if (dd.isConstant() || seenDDs.contains(dd)) {
            return;
        }
        seenDDs.add(dd);
        String name = dd.readName();
        //TODO: need to handle all cases
        if (name.contains("_LT_")) {
            fillPredicateNamingReal(name, "_LT_", predicateNaming);
        } else if (name.contains("_LEQ_")) {
            fillPredicateNamingReal(name, "_LEQ_", predicateNaming);
        } else if (name.contains("_GEQ_")) {
            fillPredicateNamingReal(name, "_GEQ_", predicateNaming);
        } else if (name.contains("_GT_")) {
            fillPredicateNamingReal(name, "_GT_", predicateNaming);
        } else if (name.contains("_EQ_")) {
            fillPredicateNamingNominal(name, "_EQ_", predicateNaming);
        } else {
            throw new IllegalArgumentException("Predicate " + name + " cannot be parsed");
        }
        fillPredicateNaming(dd.t(), predicateNaming, seenDDs);
        fillPredicateNaming(dd.e(), predicateNaming, seenDDs);

    }

    private static void fillPredicateNamingReal(String name, String op, PredicateNaming predicateNaming) {
        String[] split = name.split(op);
        String arg = split[0];
        //TODO: rhs might contain _MINUS_
        double rhs = Double.parseDouble(split[1].replace("_", "."));
        RealPredicate predicate = new RealPredicate(arg, getComparator(op), rhs);
        String predicateName = predicateNaming.name(predicate);
        if (!name.equals(predicateName)) {
            System.out.println(name + " -> " + predicateName);
            throw new IllegalArgumentException(name + " != " + predicateName);
        }
    }

    private static void fillPredicateNamingNominal(String name, String op, PredicateNaming predicateNaming) {
        String[] split = name.split(op);
        String arg = split[0];
        String rhs = split[1];
        NominalPredicate predicate = new NominalPredicate(arg, getNominalComparator(op), rhs);
        String predicateName = predicateNaming.name(predicate);
        if (!name.equals(predicateName)) {
            System.out.println(name + " -> " + predicateName);
            throw new IllegalArgumentException(name + " != " + predicateName);
        }
    }

    private static RealPredicate.Comparator getComparator(String op) {
        switch (op) {
            case "_LT_":
                return RealPredicate.Comparator.LT;
            case "_LEQ_":
                return RealPredicate.Comparator.LEQ;
            case "_GT_":
                return RealPredicate.Comparator.GT;
            case "_GEQ_":
                return RealPredicate.Comparator.GEQ;
            default:
                throw new IllegalArgumentException("Operator " + op + " is not supported");
        }
    }

    private static NominalPredicate.Comparator getNominalComparator(String op) {
        switch (op) {
            case "_EQ_":
                return NominalPredicate.Comparator.EQ;
            case "_NEQ_":
                return NominalPredicate.Comparator.NEQ;
            default:
                throw new IllegalArgumentException("Operator " + op + " is not supported");
        }
    }

    public static ADDInformation createADDInfoVerification(ADDInformation inputAddInfo,
                                                           String serializedDD,
                                                           String addVariant,
                                                           String errorMessage) {
        DataSet dataset = getDataSetController().createTransient("dataset");
        dataset.settype(inputAddInfo.getdataSet().gettype());
        dataset.setfile(inputAddInfo.getdataSet().getfile());

        ADDInformation addInfo = getADDInformationController().createTransient("addinfo");
        addInfo.setserializedDDs(serializedDD);
        addInfo.setvariant(addVariant);
        addInfo.seterrorMessage(errorMessage);

        addInfo.setseed(inputAddInfo.getseed());
        addInfo.setnumTrees(inputAddInfo.getnumTrees());
        addInfo.setbaggingSize(inputAddInfo.getbaggingSize());
        addInfo.setdataSet(dataset);
        addInfo.setclassLabel(inputAddInfo.getclassLabel());
        addInfo.setoptimizePredOrder(inputAddInfo.getoptimizePredOrder());
            /*
            if (api.getNumberOfNodes() > 1000) {
                return createADDInformation(
                        "ADDs with more than 1000 nodes will not be visualized. The created ADD has " +
                        api.getNumberOfNodes() + " nodes.");
            }
            */
        addInfo.setarffAttributes_ARFFAttribute(inputAddInfo.getarffAttributes_ARFFAttribute());
        addInfo.setclassLabels(inputAddInfo.getclassLabels());
        addInfo.setnumberOfNodes(inputAddInfo.getnumberOfNodes());
        addInfo.setmaximumDepth(inputAddInfo.getmaximumDepth());
        addInfo.setfilterUnsatPaths(inputAddInfo.getfilterUnsatPaths());
        addInfo.setpathLength(inputAddInfo.getpathLength());

        printArffAttributes(inputAddInfo.getarffAttributes_ARFFAttribute());
        System.out.println(inputAddInfo.getclassLabel());

        return addInfo;
    }

    private static void addPreconditions(RedundancyElimination<?> elimination, List<String> preconditions) {
        Map<String, Range<Double>> arg2Range = new HashMap<>();
        //TODO: need to merge ranges if there are multiple ranges for one argument
        for (String precondition : preconditions) {
            addPrecondition(elimination, precondition, arg2Range);
        }
    }

    private static void addPrecondition(RedundancyElimination<?> elimination,
                                        String precondition,
                                        Map<String, Range<Double>> arg2Range) {
        System.out.println("precondition: " + precondition);
        //create range
        String lhs = "";
        RealPredicate.Comparator comparator;
        double threshold = 0;
        if (precondition.contains(">=")) {
            String[] split = precondition.split(">=");
            lhs = split[0];
            threshold = Double.parseDouble(split[1]);
            comparator = RealPredicate.Comparator.GEQ;
        } else if (precondition.contains("<=")) {
            String[] split = precondition.split("<=");
            lhs = split[0];
            threshold = Double.parseDouble(split[1]);
            comparator = RealPredicate.Comparator.LEQ;
        } else if (precondition.contains(">")) {
            String[] split = precondition.split(">");
            lhs = split[0];
            threshold = Double.parseDouble(split[1]);
            comparator = RealPredicate.Comparator.GT;
        } else if (precondition.contains("<")) {
            String[] split = precondition.split("<");
            lhs = split[0];
            threshold = Double.parseDouble(split[1]);
            comparator = RealPredicate.Comparator.LT;
        } else if (precondition.contains("==")) {
            String[] split = precondition.split("==");
            lhs = split[0];
            threshold = Double.parseDouble(split[1]);
            lhs = lhs.replaceAll("\\s+", "");
            RealPredicate leq = new RealPredicate(lhs, RealPredicate.Comparator.LEQ, threshold);
            RealPredicate geq = new RealPredicate(lhs, RealPredicate.Comparator.GEQ, threshold);
            elimination.withPrecondition(lhs, leq.getRange(true));
            elimination.withPrecondition(lhs, geq.getRange(true));
            return;
        } else {
            throw new IllegalArgumentException("Precondition " + precondition + " cannot be parsed");
        }
        lhs = lhs.replaceAll("\\s+", "");
        RealPredicate realPredicate = new RealPredicate(lhs, comparator, threshold);
        elimination.withPrecondition(lhs, realPredicate.getRange(true));
    }

    public static ADDInformation generateDecisionTrees(long seed,
                                                       long numTrees,
                                                       long baggingSize,
                                                       boolean filterUnsatPaths,
                                                       boolean stepwiseAggregation,
                                                       FileReference dataSet,
                                                       DataSetType dataSetType,
                                                       boolean optimizePredicateOrder,
                                                       List<String> featureValues) {
        return generateDD(DDType.Trees,
                          seed,
                          numTrees,
                          baggingSize,
                          filterUnsatPaths,
                          stepwiseAggregation,
                          dataSet,
                          dataSetType,
                          optimizePredicateOrder,
                          featureValues);
    }

    public static ADDInformation generateDD(DDType ddType,
                                            long seed,
                                            long numTrees,
                                            long baggingSize,
                                            boolean filterUnsatPaths,
                                            boolean stepwiseAggregation,
                                            FileReference dataSet,
                                            DataSetType dataSetType,
                                            boolean optimizePredicateOrder,
                                            List<String> featureValues) {
        return generateDD(ddType,
                          seed,
                          numTrees,
                          baggingSize,
                          filterUnsatPaths,
                          stepwiseAggregation,
                          dataSet,
                          null,
                          dataSetType,
                          optimizePredicateOrder,
                          featureValues);
    }

    public static ADDInformation generateOutcomeExplanation(long seed,
                                                            long numTrees,
                                                            long baggingSize,
                                                            FileReference dataSet,
                                                            List<ARFFAttribute> featureLabels,
                                                            List<String> featureValues,
                                                            DataSetType dataSetType,
                                                            boolean optimizePredicateOrder) {
        if (containsInvalidInput(numTrees, baggingSize, dataSet, dataSetType)) {
            return getErrorResponse(numTrees, baggingSize, dataSet, dataSetType);
        }
        RandomForestAPI api = getRandomForestAPI(seed,
                                                 numTrees,
                                                 baggingSize,
                                                 true,
                                                 true,
                                                 dataSet,
                                                 dataSetType,
                                                 optimizePredicateOrder);
        XDDManager<Integer> ddManager = new XDDManager<>();
        try {
            XDD<Integer> majorityVoteDD = api.getMajorityVoteDD(ddManager);
            String predictedClass = api.getPredictedClass(majorityVoteDD, featureLabels, featureValues);
            XDD<Integer> classDD = api.getClassCharacterizationDD(ddManager, predictedClass, majorityVoteDD);
            BiMap<Integer, String> labelIdx = api.getLabelIdx();
            labelIdx.put(-1, "NOT " + predictedClass);
            info.scce.dime.app.forest.generator.DotGenerator<Integer> generator =
                    new info.scce.dime.app.forest.generator.DotGenerator<>(classDD,
                                                                           api.getPredNaming(),
                                                                           new IntegerPrettifier(),
                                                                           api.getLabelIdx(),
                                                                           predictedClass);
            String generatedDot = generator.getOutcomeExplanationDot(featureLabels, featureValues);
            api.setPathLength(generator.getPathLength());
            ddManager.quit();
            return createADDInformation("Outcome Expl.(" + predictedClass + ")",
                                        generatedDot,
                                        api,
                                        "",
                                        seed,
                                        numTrees,
                                        baggingSize,
                                        dataSet,
                                        dataSetType,
                                        null,
                                        optimizePredicateOrder,
                                        null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static ADDInformation generateModelExplanation(long seed,
                                                          long numTrees,
                                                          long baggingSize,
                                                          boolean stepwiseAggregation,
                                                          FileReference dataSet,
                                                          DataSetType dataSetType,
                                                          boolean optimizePredicateOrder,
                                                          List<String> featureValues) {
        return generateDD(DDType.ModelExplanation,
                          seed,
                          numTrees,
                          baggingSize,
                          true,
                          stepwiseAggregation,
                          dataSet,
                          null,
                          dataSetType,
                          optimizePredicateOrder,
                          featureValues);

    }

    public static ADDInformation generateDD(DDType ddType,
                                            long seed,
                                            long numTrees,
                                            long baggingSize,
                                            boolean filterUnsatPaths,
                                            boolean stepwiseAggregation,
                                            FileReference dataSet,
                                            String classLabel,
                                            DataSetType dataSetType,
                                            boolean optimizePredicateOrder,
                                            List<String> featureValues) {
        System.out.println("feature value: " + featureValues);
        if (containsInvalidInput(numTrees, baggingSize, dataSet, dataSetType)) {
            return getErrorResponse(numTrees, baggingSize, dataSet, dataSetType);
        }
        RandomForestAPI api = getRandomForestAPI(seed,
                                                 numTrees,
                                                 baggingSize,
                                                 filterUnsatPaths,
                                                 stepwiseAggregation,
                                                 dataSet,
                                                 dataSetType,
                                                 optimizePredicateOrder);
        String serializedDD = "";
        String addVariant = "";
        String serializedADD = "";
        try {
            switch (ddType) {
                case Trees:
                    serializedDD = api.generateDecisionTrees(featureValues);
                    addVariant = "ADD Forest";
                    break;
                case VectorADD:
                    serializedDD = api.generateVectorDD(featureValues);
                    addVariant = "Vector ADD";
                    break;
                case MajorityVote:
                    serializedDD = api.generateMajorityVoteDD(featureValues);
                    addVariant = "Majority Vote";
                    break;
                case ClassADD:
                    serializedDD = api.generateClassCharacterization(classLabel, featureValues);
                    addVariant = "Class Charac. " + classLabel;
                    break;
                case ModelExplanation:
                    serializedDD = api.generateMajorityVoteDD(featureValues);
                    addVariant = "Model Explanation";
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (filterUnsatPaths) {
            addVariant += " (filtered)";
        }

        serializedADD = api.getSerializedDD();

        return createADDInformation(addVariant,
                                    serializedDD,
                                    api,
                                    "",
                                    seed,
                                    numTrees,
                                    baggingSize,
                                    dataSet,
                                    dataSetType,
                                    classLabel,
                                    optimizePredicateOrder,
                                    serializedADD);
    }

    private static boolean containsInvalidInput(long numTrees,
                                                long baggingSize,
                                                FileReference dataSet,
                                                DataSetType dataSetType) {
        boolean invalidInput = numTrees <= 0 || numTrees > 100;
        invalidInput |= baggingSize < 0;
        invalidInput |= dataSetType.equals(DataSetType.UPLOAD) && dataSet == null;
        return invalidInput;
    }

    private static ADDInformation getErrorResponse(long numTrees,
                                                   long baggingSize,
                                                   FileReference dataSet,
                                                   DataSetType dataSetType) {
        if (numTrees <= 0 || numTrees > 100) {
            return createADDInformation("The number of trees to be learned must be between 1 and 20");
        }
        if (baggingSize < 0) {
            return createADDInformation("The bagging size must be > 0");
        }
        if (dataSetType.equals(DataSetType.UPLOAD) && dataSet == null) {
            return createADDInformation(
                    "You have to import a dataset (in the .arff format) or load an exemplary dataset by clicking on the 'Load Example Dataset' button.");
        }
        return null;
    }

    public static ADDInformation createADDInformation(String errorMessage) {
        return createADDInformation("ERROR", "", null, errorMessage, 42l, 1l, 1l, null, null, null, false, null);
    }

    public static ADDInformation createADDInformation(String addVariant,
                                                      String serializedDD,
                                                      RandomForestAPI api,
                                                      String errorMessage,
                                                      long seed,
                                                      long numTrees,
                                                      long baggingSize,
                                                      FileReference dataSet,
                                                      DataSetType dataSetType,
                                                      String classLabel,
                                                      boolean optimizePredicateOrder,
                                                      String serializedADD) {
        DataSet dataset = getDataSetController().createTransient("dataset");
        dataset.settype(dataSetType);
        dataset.setfile(dataSet);

        ADDInformation addInfo = getADDInformationController().createTransient("addinfo");
        addInfo.setserializedDDs(serializedDD);
        addInfo.setserializedADD(serializedADD);
        addInfo.setvariant(addVariant);
        addInfo.seterrorMessage(errorMessage);
        addInfo.setseed(seed);
        addInfo.setnumTrees(numTrees);
        addInfo.setbaggingSize(baggingSize);
        addInfo.setdataSet(dataset);
        addInfo.setclassLabel(classLabel);
        addInfo.setoptimizePredOrder(optimizePredicateOrder);
        if (api != null) {
            /*
            if (api.getNumberOfNodes() > 1000) {
                return createADDInformation(
                        "ADDs with more than 1000 nodes will not be visualized. The created ADD has " +
                        api.getNumberOfNodes() + " nodes.");
            }
            */
            addInfo.setarffAttributes_ARFFAttribute(api.getArffAttributes());
            addInfo.setclassLabels(api.getClassLabels());
            addInfo.setnumberOfNodes((long) api.getNumberOfNodes());
            addInfo.setmaximumDepth((long) api.getMaximumDepth());
            addInfo.setfilterUnsatPaths(api.isFilterUnsatPaths());
            addInfo.setstepwiseAggregation(api.isStepwiseAggregation());

            addInfo.setpathLength((long) api.getPathLength());

            Set<String> entries = new HashSet<>();
            for (Map.Entry<Integer, String> entry : api.getLabelIdx().entrySet()) {
                String labelIdx = entry.getKey() + "#" + entry.getValue();
                entries.add(labelIdx);
            }
            String serializedLabelIdx = String.join(",", entries);
            addInfo.setlabelIdx(serializedLabelIdx);
            System.out.println("serialized labelIdx: " + serializedLabelIdx);

            printArffAttributes(api.getArffAttributes());
            System.out.println(api.getClassLabels());

        }

        return addInfo;
    }

    public static RandomForestAPI getRandomForestAPI(long seed,
                                                     long numTrees,
                                                     long baggingSize,
                                                     boolean filterUnsatPaths,
                                                     boolean stepwiseAggregation,
                                                     FileReference dataSet,
                                                     DataSetType dataSetType,
                                                     boolean optimizePredicateOrder) {
        InputStream inputStream = null;

        System.out.println(dataSet);
        System.out.println(dataSetType);

        switch (dataSetType) {
            case BALANCE_SCALE:
                inputStream = Native.class.getResourceAsStream("balance-scale.arff");
                break;
            case BREAST_CANCER:
                inputStream = Native.class.getResourceAsStream("breast-cancer.arff");
                break;
            case IRIS:
                inputStream = Native.class.getResourceAsStream("iris.arff");
                break;
            case LENSES:
                inputStream = Native.class.getResourceAsStream("lenses.arff");
                break;
            case TIC_TAC_TOE:
                inputStream = Native.class.getResourceAsStream("tic-tac-toe.arff");
                break;
            case UPLOAD:
                DomainFileController dfc = getDomainFileController();
                inputStream = dfc.loadFile(dataSet);
                break;
            case VOTE:
                inputStream = Native.class.getResourceAsStream("vote.arff");
                break;
            default:
                throw new RuntimeException("Unknown value for DataSetType");
        }

        return new RandomForestAPI((int) seed,
                                   (int) baggingSize,
                                   (int) numTrees,
                                   filterUnsatPaths,
                                   stepwiseAggregation,
                                   inputStream,
                                   optimizePredicateOrder);
    }

    public static ADDInformationController getADDInformationController() {
        if (addInfoController == null) {
            BeanManager bm = CDI.current().getBeanManager();
            Set<Bean<?>> beans = bm.getBeans(ADDInformationController.class);
            Bean<ADDInformationController> bean = (Bean<ADDInformationController>) bm.resolve(beans);
            CreationalContext<ADDInformationController> cctx = bm.createCreationalContext(bean);
            addInfoController = (ADDInformationController) bm.getReference(bean, ADDInformationController.class, cctx);
        }
        return addInfoController;
    }

    public static DataSetController getDataSetController() {
        if (dataSetController == null) {
            BeanManager bm = CDI.current().getBeanManager();
            Set<Bean<?>> beans = bm.getBeans(DataSetController.class);
            Bean<DataSetController> bean = (Bean<DataSetController>) bm.resolve(beans);
            CreationalContext<DataSetController> cctx = bm.createCreationalContext(bean);
            dataSetController = (DataSetController) bm.getReference(bean, DataSetController.class, cctx);
        }
        return dataSetController;
    }

    public static DomainFileController getDomainFileController() {
        if (dfc == null) {
            BeanManager bm = CDI.current().getBeanManager();
            Set<Bean<?>> beans = bm.getBeans(DomainFileController.class);
            Bean<DomainFileController> bean = (Bean<DomainFileController>) bm.resolve(beans);
            CreationalContext<DomainFileController> cctx = bm.createCreationalContext(bean);
            dfc = (DomainFileController) bm.getReference(bean, DomainFileController.class, cctx);
        }
        return dfc;
    }

    public static long randomLong() {
        return new Random().nextLong();
    }

    public static ADDInformation generateVectorDD(long seed,
                                                  long numTrees,
                                                  long baggingSize,
                                                  boolean filterUnsatPaths,
                                                  boolean stepwiseAggregation,
                                                  FileReference dataSet,
                                                  DataSetType dataSetType,
                                                  boolean optimizePredicateOrder,
                                                  List<String> featureValues) {
        if (numTrees > 10) {
            return createADDInformation(
                    "For the Vector ADD, the number of trees is limited to 10 as the ADD will be too large to visualize otherwise.");
        }
        return generateDD(DDType.VectorADD,
                          seed,
                          numTrees,
                          baggingSize,
                          filterUnsatPaths,
                          stepwiseAggregation,
                          dataSet,
                          dataSetType,
                          optimizePredicateOrder,
                          featureValues);
    }

    public static ADDInformation generateMajorityVoteDD(long seed,
                                                        long numTrees,
                                                        long baggingSize,
                                                        boolean filterUnsatPaths,
                                                        boolean stepwiseAggregation,
                                                        FileReference dataSet,
                                                        DataSetType dataSetType,
                                                        boolean optimizePredicateOrder,
                                                        List<String> featureValues) {
        return generateDD(DDType.MajorityVote,
                          seed,
                          numTrees,
                          baggingSize,
                          filterUnsatPaths,
                          stepwiseAggregation,
                          dataSet,
                          dataSetType,
                          optimizePredicateOrder,
                          featureValues);
    }

    public static ADDInformation generateClassCharacterization(long seed,
                                                               long numTrees,
                                                               long baggingSize,
                                                               boolean filterUnsatPaths,
                                                               boolean stepwiseAggregation,
                                                               FileReference dataSet,
                                                               String classLabel,
                                                               DataSetType dataSetType,
                                                               boolean optimizePredicateOrder,
                                                               List<String> featureValues) {
        return generateDD(DDType.ClassADD,
                          seed,
                          numTrees,
                          baggingSize,
                          filterUnsatPaths,
                          stepwiseAggregation,
                          dataSet,
                          classLabel,
                          dataSetType,
                          optimizePredicateOrder,
                          featureValues);
    }

    public static String randomUUID() {
        return java.util.UUID.randomUUID().toString();
    }

    public static FileReference javaGenerator(long seed,
                                              long numTrees,
                                              long baggingSize,
                                              boolean filterUnsatPaths,
                                              boolean stepwiseAggregation,
                                              FileReference dataSet,
                                              String chosenMethod,
                                              DataSetType dataSetType,
                                              boolean optimizePredicateOrder) {
        return generate(GeneratorType.JAVA,
                        seed,
                        numTrees,
                        baggingSize,
                        filterUnsatPaths,
                        stepwiseAggregation,
                        dataSet,
                        chosenMethod,
                        dataSetType,
                        optimizePredicateOrder);
    }

    public static FileReference generate(GeneratorType generatorType,
                                         long seed,
                                         long numTrees,
                                         long baggingSize,
                                         boolean filterUnsatPaths,
                                         boolean stepwiseAggregation,
                                         FileReference dataSet,
                                         String chosenMethod,
                                         DataSetType dataSetType,
                                         boolean optimizePredicateOrder) {
        if (containsInvalidInput(numTrees, baggingSize, dataSet, dataSetType)) {
            return null;
        }
        if (chosenMethod.equals("modelexplanation") || chosenMethod.startsWith("class")) {
            filterUnsatPaths = true;
        }
        RandomForestAPI api = getRandomForestAPI(seed,
                                                 numTrees,
                                                 baggingSize,
                                                 filterUnsatPaths,
                                                 stepwiseAggregation,
                                                 dataSet,
                                                 dataSetType,
                                                 optimizePredicateOrder);
        try {
            String generatedCode = "";
            String fileName = "";
            List<XDD<?>> dds = new ArrayList<>();
            if (chosenMethod.equals("trees")) {
                MonoidDDManager<LabelVector> ddManager = new LabelVectorDDManager();
                List<XDD<LabelVector>> dts = api.getDTs(ddManager);
                List<LabelledRegularDD<XDD<LabelVector>>> labelledDts = new ArrayList<>();
                for (int i = 0; i < dts.size(); i++) {
                    labelledDts.add(new LabelledRegularDD<>(dts.get(i), "Tree" + (i + 1)));
                }
                dds.addAll(dts);
                generatedCode = generate(generatorType, labelledDts, fileName);
                fileName = "ADDForest";
            } else if (chosenMethod.equals("vector")) {
                LabelMultisetDDManager ddManager = new LabelMultisetDDManager();
                XDD<LabelMultiset> xdd = api.getVectorADD(ddManager, optimizePredicateOrder);
                fileName = "VectorADD";
                generatedCode = generate(generatorType, xdd, fileName);
                dds.add(xdd);
            } else if (chosenMethod.equals("majorityvote")) {
                XDDManager<Integer> xddManager = new XDDManager<>();
                XDD<Integer> xdd = api.getMajorityVoteDD(xddManager);
                fileName = "MajorityVote";
                generatedCode = generate(generatorType, xdd, fileName);
                dds.add(xdd);
            } else if (chosenMethod.equals("modelexplanation")) {
                XDDManager<Integer> xddManager = new XDDManager<>();
                XDD<Integer> xdd = api.getMajorityVoteDD(xddManager);
                fileName = "ModelExplanation";
                generatedCode = generate(generatorType, xdd, fileName);
                dds.add(xdd);
            } else {
                String[] split = chosenMethod.split("#");
                String classLabel = split[1].replaceAll("\\s+", "");
                XDDManager<Integer> xddManager = new XDDManager<>();
                XDD<Integer> xdd = api.getClassCharacterizationDD(xddManager, classLabel);
                fileName = "ClassDD";
                generatedCode = generate(generatorType, xdd, "ClassDD");
                dds.add(xdd);
            }
            PredicatesGenerator predicatesGenerator =
                    getPredicatesGenerator(generatorType, dds, api.getPredNaming(), api.getArffAttributes());
            List<String> fileNames = predicatesGenerator.getFileNames();
            List<String> fileContents = fixedFileContents(predicatesGenerator.getFileContents());
            String fileEnding = getFileEnding(generatorType);
            fileNames.add(fileName + "." + fileEnding);
            fileContents.add(generatedCode);
            return createZip(getZipName(generatorType) + ".zip", fileNames, fileContents);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static List<String> fixedFileContents(List<String> fileContents) {
        // generated code uses names of ARFFAttributes as variable names which sometimes contain hyphens
        List<String> fixedFileContents = new ArrayList<>();
        for (String fileContent : fileContents) {
            String fixedFileContent = fileContent.replace("-", "_");
            fixedFileContents.add(fixedFileContent);
        }
        return fixedFileContents;
    }

    public static FileReference createZip(String zipName, List<String> fileNames, List<String> fileContents)
            throws IOException {
        if (fileNames.size() != fileContents.size()) {
            throw new IllegalArgumentException(
                    "fileNames' and fileContents' size has to be equal: fileNames.size()=" + fileNames.size() +
                    " while fileContents.size()=" + fileContents.size());
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ZipOutputStream zos = new ZipOutputStream(out);
        for (int i = 0; i < fileNames.size(); i++) {
            String fileName = fileNames.get(i);
            String fileContent = fileContents.get(i);
            PrintWriter writer = new PrintWriter(fileName, "UTF-8");
            writer.println(fileContent);
            writer.close();

            try (FileInputStream in = new FileInputStream(fileName)) {
                zos.putNextEntry(new ZipEntry(fileName));
                byte[] b = new byte[1024];
                int count;
                while ((count = in.read(b)) > 0) {
                    zos.write(b, 0, count);
                }
                zos.closeEntry();
            }
        }
        zos.close();
        InputStream inputStream = new ByteArrayInputStream(out.toByteArray());
        return getDomainFileController().storeFile(zipName, inputStream);
    }

    public static <T extends RegularDD<?, T>> String generate(GeneratorType generatorType,
                                                              List<LabelledRegularDD<T>> dds,
                                                              String className) {
        CodeGenerator<T> generator = null;
        switch (generatorType) {
            case JAVA:
                generator = new JavaGenerator<T>();
                break;
            case CPP:
                generator = new CPPGenerator<T>().withParamType("PredicatesDefault");
                break;
            case PYTHON:
                generator = new PythonGenerator<T>();
                break;
            case DOT:
                generator = new DotGenerator<>();
        }
        String generatedCode = generator.generateToString(dds);
        if (generatorType == GeneratorType.CPP) {
            //TODO: Fix this in the ADD-Lib
            generatedCode = generatedCode.replace("Predicates predicates = pPredicates;", "");
            generatedCode = generatedCode.replace("pPredicates", "predicates");
        }
        return generatedCode;
    }

    public static String getZipName(GeneratorType generatorType) {
        switch (generatorType) {
            case JAVA:
                return "java";
            case CPP:
                return "cpp";
            case PYTHON:
                return "python";
            case DOT:
                return "dot";
        }
        return "";
    }

    public static PredicatesGenerator getPredicatesGenerator(GeneratorType generatorType,
                                                             List<XDD<?>> dds,
                                                             PredicateNaming predNaming,
                                                             List<ARFFAttribute> arffAttributes) {
        switch (generatorType) {
            case JAVA:
                return new PredicatesGeneratorJava(dds, predNaming, arffAttributes);
            case CPP:
                return new PredicatesGeneratorCPP(dds, predNaming, arffAttributes);
            case PYTHON:
                return new PredicatesGeneratorPython(dds, predNaming, arffAttributes);
            case DOT:
                return new PredicatesGeneratorEmpty();
        }
        return new PredicatesGeneratorEmpty();
    }

    public static <T extends RegularDD<?, T>> String generate(GeneratorType generatorType, T dd, String name) {
        List<LabelledRegularDD<T>> labelledDDs = new ArrayList<>();
        LabelledRegularDD<T> labelledDD = new LabelledRegularDD<>(dd, name);
        labelledDDs.add(labelledDD);
        return generate(generatorType, labelledDDs, name);
    }

    public static String getFileEnding(GeneratorType generatorType) {
        switch (generatorType) {
            case JAVA:
                return "java";
            case CPP:
                return "h";
            case PYTHON:
                return "py";
            case DOT:
                return "dot";
        }
        return "";
    }

    public static String realListToString(List<Double> values) {
        List<String> strings = new ArrayList<>();
        for (Double val : values) {
            strings.add(val.toString());
        }
        return stringListToString(strings);
    }

    public static String stringListToString(List<String> strings) {
        return String.join(",", strings);
    }

    public static boolean isNotNullOrEmpty(String message) {
        return message != null && !message.isEmpty();
    }

    public static List<ADDInformation> bringADDInformationToTop(List<ADDInformation> addInformations,
                                                                ADDInformation addInformation) {
        List<ADDInformation> history = new ArrayList<>(addInformations);
        history.remove(addInformation);
        history.add(0, addInformation);
        return history;
    }

    public static List<ADDInformation> reverseADDInformations(List<ADDInformation> addInformations) {
        if (addInformations.isEmpty()) {return new ArrayList<>();}
        List<ADDInformation> reversedADDInformations = new ArrayList<>();
        reversedADDInformations.add(addInformations.get(addInformations.size() - 1));
        for (int i = 0; i < addInformations.size() - 1; i++) {
            reversedADDInformations.add(addInformations.get(i));
        }

        // remove duplicate, can be improved to O(n)
        int removeIdx = -1;
        for (int i = 0; i < reversedADDInformations.size() - 1; i++) {
            ADDInformation info1 = reversedADDInformations.get(i);
            for (int j = i + 1; j < reversedADDInformations.size(); j++) {
                ADDInformation info2 = reversedADDInformations.get(j);
                if (!info1.getvariant().equals(info2.getvariant())) {continue;}
                if (!info1.getnumberOfNodes().equals(info2.getnumberOfNodes())) {continue;}
                if (!info1.getmaximumDepth().equals(info2.getmaximumDepth())) {continue;}
                if (!info1.getnumTrees().equals(info2.getnumTrees())) {continue;}
                if (!info1.getbaggingSize().equals(info2.getbaggingSize())) {continue;}
                if (!info1.getseed().equals(info2.getseed())) {continue;}
                if (!info1.getfilterUnsatPaths().equals(info2.getfilterUnsatPaths())) {continue;}
                if (!datasetEquals(info1.getdataSet(), info2.getdataSet())) {continue;}
                if (!info1.getoptimizePredOrder().equals(info2.getoptimizePredOrder())) {continue;}
                removeIdx = j;
                i = reversedADDInformations.size(); // break outer loop
                break;
            }
        }
        if (removeIdx != -1) {
            reversedADDInformations.remove(removeIdx);
        }
        return reversedADDInformations;
    }

    public static boolean datasetEquals(DataSet set1, DataSet set2) {
        if (set1.getfile() == null) {
            if (set2.getfile() != null) {
                return false;
            }
            return set1.gettype().equals(set2.gettype());
        }
        if (set2.getfile() == null) {
            return false;
        }
        return set1.getfile().getFileName().equals(set2.getfile().getFileName());
    }

    public static boolean isNullOrEmpty(List<ADDInformation> addInformations) {
        return addInformations == null || addInformations.isEmpty();
    }

    public static FileReference history2CSV(List<ADDInformation> history) {
        String fileName = "history";
        String fileEnding = "csv";
        String columnNames = getHistoryColumnNames();
        List<String> serializedHistory =
                history.stream().map(Native::serializeADDInformation).collect(Collectors.toList());
        String historyValues = String.join(System.lineSeparator(), serializedHistory);
        String generatedCSV = columnNames + System.lineSeparator() + historyValues;
        return getDomainFileController().storeFile(fileName + "." + fileEnding,
                                                   new ByteArrayInputStream(generatedCSV.getBytes()));
    }

    public static String getHistoryColumnNames() {
        List<String> values = new ArrayList<>(Arrays.asList("ADD Variant",
                                                            "Number of nodes",
                                                            "Maximum depth",
                                                            "Number of trees",
                                                            "Bagging size",
                                                            "Seed",
                                                            "Filter unsat paths",
                                                            "Dataset"));
        return String.join(",", values);
    }

    public static String serializeADDInformation(ADDInformation addInformation) {
        List<String> values = new ArrayList<>(Arrays.asList(addInformation.getvariant(),
                                                            "" + addInformation.getnumberOfNodes(),
                                                            "" + addInformation.getmaximumDepth(),
                                                            "" + addInformation.getnumTrees(),
                                                            "" + addInformation.getbaggingSize(),
                                                            "" + addInformation.getseed(),
                                                            "" + addInformation.getfilterUnsatPaths(),
                                                            "" + addInformation.getdataSet().gettype()));
        return String.join(",", values);
    }

    public static List<ADDInformation> deleteHistoryEntry(List<ADDInformation> history, Long ts) {
        int idxToBeDeleted = -1;
        for (int i = 0; i < history.size(); i++) {
            if (history.get(i).gettimestamp().equals(ts)) {
                idxToBeDeleted = i;
                break;
            }
        }
        if (idxToBeDeleted != -1) {
            history.remove(idxToBeDeleted);
        }
        return history;
    }

    public static ADDInformation findHistoryEntry(List<ADDInformation> history, Long timestamp) {
        for (int i = 0; i < history.size(); i++) {
            if (history.get(i).gettimestamp().equals(timestamp)) {
                return history.get(i);
            }
        }
        return null;
    }

    public static void printAddInformation(ADDInformation info) {
        System.out.println(serializeADDInformation(info));
    }

    public static boolean fileExists(FileReference file) {
        return file != null;
    }

    public static List<String> getArffAttributeNames(List<ARFFAttribute> arffAttributes) {
        System.out.println("getArffAttributeNames");
        printArffAttributes(arffAttributes);
        return arffAttributes.stream().map(ARFFAttribute::getname).collect(Collectors.toList());
    }

    public static List<String> getArffAttributeTypes(List<ARFFAttribute> arffAttributes) {
        System.out.println("getArffAttributeTypes");
        printArffAttributes(arffAttributes);
        return arffAttributes.stream()
                             .map(ARFFAttribute::getarffDataype)
                             .map(ARFFDatatype::toString)
                             .collect(Collectors.toList());
    }

    private static void printArffAttributes(List<ARFFAttribute> arffAttributes) {
        for (ARFFAttribute attribute : arffAttributes) {
            System.out.println("name:" + attribute.getname());
            System.out.println("type:" + attribute.getarffDataype());
        }
    }

    enum DDType {
        Trees,
        VectorADD,
        MajorityVote,
        ClassADD,
        ModelExplanation
    }

}