package info.scce.dime.app.forest.generator;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.google.common.collect.BiMap;
import de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.app.ARFFAttribute;
import info.scce.addlib.dd.DD;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.infrandforest.assignment.NominalPredicate;
import info.scce.addlib.infrandforest.assignment.Predicate;
import info.scce.addlib.infrandforest.assignment.RealPredicate;
import info.scce.addlib.infrandforest.dd.PredicateNaming;
import info.scce.dime.app.forest.LeafLabelPrettifier;

public class DotGenerator<T> {

    private final List<XDD<T>> xdds;
    private final PredicateNaming predNaming;
    private final Map<XDD<T>, Long> dd2Id;
    private final LeafLabelPrettifier<T> prettifier;
    private final BiMap<Integer, String> labelIdx;
    private final List<String> rootNames;

    private final Set<XDD<T>> unsatisfiedPredicates;
    private final Set<XDD<T>> satisfiedPredicates;
    private final Set<XDD<T>> redundantPredicates;

    private boolean isClassChar;
    private boolean highlightPath;
    private String classLabel;
    private int pathLength;

    public DotGenerator(XDD<T> dd,
                        PredicateNaming predNaming,
                        LeafLabelPrettifier<T> prettifier,
                        BiMap<Integer, String> labelIdx,
                        String rootName) {
        this(Collections.singletonList(dd), predNaming, prettifier, labelIdx, Collections.singletonList(rootName));
    }

    public DotGenerator(List<XDD<T>> xdds,
                        PredicateNaming predNaming,
                        LeafLabelPrettifier<T> prettifier,
                        BiMap<Integer, String> labelIdx,
                        List<String> rootNames) {
        this.xdds = xdds;
        this.predNaming = predNaming;
        this.prettifier = prettifier;
        this.labelIdx = labelIdx;
        this.rootNames = rootNames;
        this.dd2Id = new HashMap<>();
        this.unsatisfiedPredicates = new HashSet<>();
        this.satisfiedPredicates = new HashSet<>();
        this.redundantPredicates = new HashSet<>();
    }

    public void setClassLabel(String classLabel) {
        this.isClassChar = true;
        this.classLabel = classLabel;
    }

    public String generateDot() {
        StringBuilder stringBuilder = new StringBuilder();
        appendHead(stringBuilder);
        Set<XDD<T>> seenDDs = new HashSet<>();
        int i = 0;
        for (XDD<T> dd : xdds) {
            appendNode(stringBuilder, dd, seenDDs);
            stringBuilder.append(getRootDot("root" + i, rootNames.get(i), getId(dd)));
            i++;
        }
        stringBuilder.append("}");
        return stringBuilder.toString();
    }

    private String getRootDot(String id, String label, String targetId) {
        String node = "\"" + id + "\"" + " [fillcolor = lightgray, shape = rectangle, label = \"" + label + "\"]\n";
        String edge = "\"" + id + "\"" + " -> " + "\"" + targetId + "\"\n";
        return node + edge;
    }

    private void appendNode(StringBuilder stringBuilder, XDD<T> dd, Set<XDD<T>> seenDDs) {
        if (!seenDDs.contains(dd)) {
            seenDDs.add(dd);
            String color = getColor(dd);
            String id = getId(dd);
            String label = getLabel(dd);
            if (dd.isConstant()) {
                stringBuilder.append(leafDot(id, color, label));
                stringBuilder.append(System.lineSeparator());
            } else {
                stringBuilder.append(innerNodeDot(id, color, label));
                stringBuilder.append(System.lineSeparator());
                stringBuilder.append(trueEdge(id, getId(dd.t()), getPenWidthTrueEdge(dd)));
                stringBuilder.append(System.lineSeparator());
                stringBuilder.append(falseEdge(id, getId(dd.e()), getPenWidthFalseEdge(dd)));
                stringBuilder.append(System.lineSeparator());
                appendNode(stringBuilder, dd.t(), seenDDs);
                appendNode(stringBuilder, dd.e(), seenDDs);
            }
        }
    }

    private String getId(XDD<T> dd) {
        return "" + dd2Id.computeIfAbsent(dd, DD::ptr);
    }

    private String getColor(XDD<T> dd) {
        if (!highlightPath && dd.isConstant() && isClassChar) {
            String label = getLabel(dd);
            if (label.equals(classLabel)) {
                return "darkseagreen";
            } else {
                return "salmon";
            }
        }
        if (redundantPredicates.contains(dd)) {
            return "lightskyblue1";
        }
        if (satisfiedPredicates.contains(dd)) {
            return "darkseagreen";
        }
        if (unsatisfiedPredicates.contains(dd)) {
            return "salmon";
        }
        return "white";
    }

    private String getLabel(XDD<T> dd) {
        if (dd.isConstant()) {
            return prettifier.prettifyLabel(dd.v(), labelIdx);
        } else {
            String label = dd.readName();
            Predicate predicate = predNaming.predicate(label);
            String argument = predicate.argument();
            if (predicate instanceof NominalPredicate) {
                String referenceValue = ((NominalPredicate) predicate).referenceValue();
                return argument + " == " + referenceValue;
            } else if (predicate instanceof RealPredicate) {
                double threshold = ((RealPredicate) predicate).threshold();
                return argument + " < " + threshold;
            } else {
                throw new IllegalArgumentException("Unsupported predicate type.");
            }
        }
    }

    private String getPenWidthTrueEdge(XDD<T> dd) {
        return satisfiedPredicates.contains(dd) ? "3" : "1";
    }

    private String getPenWidthFalseEdge(XDD<T> dd) {
        return unsatisfiedPredicates.contains(dd) ? "3" : "1";
    }

    private void appendHead(StringBuilder stringBuilder) {
        stringBuilder.append("digraph \"Graph\" {");
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append("\tbgcolor = transparent");
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append("\tnode [style = filled, fontsize = 14, fontname = Arial]");
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append("\tedge [arrowhead = none]");
        stringBuilder.append(System.lineSeparator());
    }

    public String getOutcomeExplanationDot(List<ARFFAttribute> attributes, List<String> attributeValues) {
        highlightPath = true;
        checkAttributes(attributes, attributeValues);
        calculatePath(attributes, attributeValues, true);
        return generateDot();
    }

    public String showPathDot(List<ARFFAttribute> attributes, List<String> attributeValues) {
        highlightPath = true;
        checkAttributes(attributes, attributeValues);
        calculatePath(attributes, attributeValues, false);
        return generateDot();
    }

    private void checkAttributes(List<ARFFAttribute> attributes, List<String> attributeValues) {
        if (attributes.size() != attributeValues.size()) {
            throw new IllegalArgumentException(
                    "There are " + attributes.size() + " attributes but " + attributeValues.size() +
                    " attribute values");
        }
    }

    private void calculatePath(List<ARFFAttribute> attributes,
                               List<String> attributeValues,
                               boolean highlightRedundantPredicates) {
        Map<String, Double> attribute2NumericValue = new HashMap<>();
        Map<String, String> attribute2NominalValue = new HashMap<>();
        fillAttributeMaps(attributes, attributeValues, attribute2NumericValue, attribute2NominalValue);
        xdds.forEach(dd -> calculatePath(dd,
                                         attribute2NumericValue,
                                         attribute2NominalValue,
                                         highlightRedundantPredicates));
    }

    private void fillAttributeMaps(List<ARFFAttribute> attributes,
                                   List<String> attributeValues,
                                   Map<String, Double> attribute2NumericValue,
                                   Map<String, String> attribute2NominalValue) {
        for (int i = 0; i < attributes.size(); i++) {
            String name = attributes.get(i).getname();
            switch (attributes.get(i).getarffDataype()) {
                case NUMERIC:
                    attribute2NumericValue.put(name, Double.parseDouble(attributeValues.get(i)));
                    break;
                case NOMINAL:
                    attribute2NominalValue.put(name, attributeValues.get(i));
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + attributes.get(i).getarffDataype());
            }
        }
    }

    private void calculatePath(XDD<T> dd,
                               Map<String, Double> attribute2NumericValue,
                               Map<String, String> attribute2NominalValue,
                               boolean highlightRedundantPredicates) {
        Map<String, Double> attribute2LowerBound = new HashMap<>();
        Map<String, Double> attribute2UpperBound = new HashMap<>();
        Map<String, String> attribute2SeenReferenceValue = new HashMap<>();
        XDD<T> root = dd;
        while (!dd.isConstant()) {
            String label = dd.readName();
            Predicate predicate = predNaming.predicate(label);
            String argument = predicate.argument();
            if (predicate instanceof NominalPredicate) {
                dd = calculatePathNominalPredicate(dd,
                                                   attribute2NominalValue,
                                                   attribute2SeenReferenceValue,
                                                   (NominalPredicate) predicate,
                                                   argument);
            } else if (predicate instanceof RealPredicate) {
                dd = calculatePathRealPredicate(dd,
                                                attribute2NumericValue,
                                                attribute2LowerBound,
                                                attribute2UpperBound,
                                                (RealPredicate) predicate,
                                                argument);
            } else {
                throw new IllegalArgumentException("Unsupported predicate type.");
            }
            pathLength++;
        }

        // predicted leaf is highlighted in green
        satisfiedPredicates.add(dd);
        if (highlightRedundantPredicates) {
            calculateRedundantPredicates(root,
                                         attribute2LowerBound,
                                         attribute2UpperBound,
                                         attribute2SeenReferenceValue);
        }
    }

    private XDD<T> calculatePathNominalPredicate(XDD<T> dd,
                                                 Map<String, String> attribute2NominalValue,
                                                 Map<String, String> attribute2SeenReferenceValue,
                                                 NominalPredicate predicate,
                                                 String argument) {
        String referenceValue = predicate.referenceValue();
        if (attribute2NominalValue.containsKey(argument)) {
            String attributeValue = attribute2NominalValue.get(argument);
            if (attributeValue.equals(referenceValue)) {
                satisfiedPredicates.add(dd);
                dd = dd.t();
                if (!attribute2SeenReferenceValue.containsKey(argument)) {
                    attribute2SeenReferenceValue.put(argument, referenceValue);
                }
            } else {
                unsatisfiedPredicates.add(dd);
                dd = dd.e();
            }
        } else {
            throw new IllegalArgumentException("No value found for nominal attribute " + argument);
        }
        return dd;
    }

    private XDD<T> calculatePathRealPredicate(XDD<T> dd,
                                              Map<String, Double> attribute2NumericValue,
                                              Map<String, Double> attribute2LowerBound,
                                              Map<String, Double> attribute2UpperBound,
                                              RealPredicate predicate,
                                              String argument) {
        XDD<T> successor;
        double threshold = predicate.threshold();
        if (attribute2NumericValue.containsKey(argument)) {
            double attributeValue = attribute2NumericValue.get(argument);
            if (attributeValue < threshold) {
                satisfiedPredicates.add(dd);
                successor = dd.t();
                if (attribute2UpperBound.containsKey(argument)) {
                    double upperBound = Math.min(attribute2UpperBound.get(argument), threshold);
                    attribute2UpperBound.put(argument, upperBound);
                } else {
                    attribute2UpperBound.put(argument, threshold);
                }
            } else {
                unsatisfiedPredicates.add(dd);
                successor = dd.e();
                if (attribute2LowerBound.containsKey(argument)) {
                    double lowerBound = Math.max(attribute2LowerBound.get(argument), threshold);
                    attribute2LowerBound.put(argument, lowerBound);
                } else {
                    attribute2LowerBound.put(argument, threshold);
                }
            }
        } else {
            throw new IllegalArgumentException("No value found for numeric attribute " + argument);
        }
        return successor;
    }

    private void calculateRedundantPredicates(XDD<T> dd,
                                              Map<String, Double> attribute2LowerBound,
                                              Map<String, Double> attribute2UpperBound,
                                              Map<String, String> attribute2SeenReferenceValue) {
        while (!dd.isConstant()) {
            String label = dd.readName();
            Predicate predicate = predNaming.predicate(label);
            String argument = predicate.argument();
            if (predicate instanceof NominalPredicate) {
                calculcateRedundantPredicateNominal(dd,
                                                    attribute2SeenReferenceValue,
                                                    (NominalPredicate) predicate,
                                                    argument);
            } else if (predicate instanceof RealPredicate) {
                calculcateRedundantPredicateReal(dd,
                                                 attribute2LowerBound,
                                                 attribute2UpperBound,
                                                 (RealPredicate) predicate,
                                                 argument);
            } else {
                throw new IllegalArgumentException("Unsupported predicate type.");
            }
            if (satisfiedPredicates.contains(dd)) {
                dd = dd.t();
            } else {
                dd = dd.e();
            }
        }
    }

    private void calculcateRedundantPredicateReal(XDD<T> dd,
                                                  Map<String, Double> attribute2LowerBound,
                                                  Map<String, Double> attribute2UpperBound,
                                                  RealPredicate predicate,
                                                  String argument) {
        double threshold = predicate.threshold();
        boolean isRedundant =
                attribute2LowerBound.containsKey(argument) && attribute2LowerBound.get(argument) > threshold;
        isRedundant |= attribute2UpperBound.containsKey(argument) && attribute2UpperBound.get(argument) < threshold;
        if (isRedundant) {
            redundantPredicates.add(dd);
        }
    }

    private void calculcateRedundantPredicateNominal(XDD<T> dd,
                                                     Map<String, String> attribute2SeenReferenceValue,
                                                     NominalPredicate predicate,
                                                     String argument) {
        String referenceValue = predicate.referenceValue();
        if (attribute2SeenReferenceValue.containsKey(argument) &&
            !attribute2SeenReferenceValue.get(argument).equals(referenceValue)) {
            redundantPredicates.add(dd);
        }
    }

    private String nodeDot(String id, String color, String label, String shape) {
        return "\"" + id + "\"" + " [fillcolor = " + color + ", shape = " + shape + ", label=\"" + label + "\"]";
    }

    private String innerNodeDot(String id, String color, String label) {
        return nodeDot(id, color, label, "ellipse");
    }

    private String leafDot(String id, String color, String label) {
        return nodeDot(id, color, label, "rect");
    }

    private String trueEdge(String id, String targetId, String penWidth) {
        return edgeDot(id, targetId, penWidth, "solid");
    }

    private String falseEdge(String id, String targetId, String penWidth) {
        return edgeDot(id, targetId, penWidth, "dashed");
    }

    private String edgeDot(String id, String targetId, String penWidth, String style) {
        return "\"" + id + "\"" + " -> " + "\"" + targetId + "\" [style = " + style + ", penwidth=" + penWidth + "]";
    }

    public int getPathLength() {
        return pathLength;
    }

}
