package info.scce.dime.app.forest.generator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.app.ARFFAttribute;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.infrandforest.assignment.NominalPredicate;
import info.scce.addlib.infrandforest.assignment.Predicate;
import info.scce.addlib.infrandforest.assignment.RealPredicate;
import info.scce.addlib.infrandforest.dd.PredicateNaming;

public class PredicatesGeneratorPython extends PredicatesGenerator {

    public PredicatesGeneratorPython(List<XDD<?>> xdds,
                                     PredicateNaming predNaming,
                                     List<ARFFAttribute> arffAttributes) {
        super(xdds, predNaming, arffAttributes);
    }

    @Override
    public List<String> getFileNames() {
        return new ArrayList<>(Arrays.asList("Predicates.py", "PredicatesDefault.py"));
    }

    @Override
    public List<String> getFileContents() {
        return new ArrayList<>(Arrays.asList(generateInterface(), generateImplementation()));
    }

    private String generateInterface() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("class Predicates:");
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append(System.lineSeparator());

        stringBuilder.append("\tdef __init__(self, ").append(getConstructorArguments()).append("):");
        stringBuilder.append(System.lineSeparator());

        for (String ddLabel : foundLabels) {
            stringBuilder.append("\t\tself.").append(ddLabel).append(" = ").append(ddLabel);
            stringBuilder.append(System.lineSeparator());
        }
        stringBuilder.append(System.lineSeparator());

        for (String ddLabel : foundLabels) {
            stringBuilder.append("\tdef ").append(ddLabel).append("(self):");
            stringBuilder.append(System.lineSeparator());
            stringBuilder.append("\t\treturn self.").append(ddLabel).append("()");
            stringBuilder.append(System.lineSeparator());
            stringBuilder.append(System.lineSeparator());
        }

        return stringBuilder.toString();
    }

    private String getConstructorArguments() {
        return String.join(", ", orderedLabels);
    }

    private String generateImplementation() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("class PredicatesDefault:");
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append(System.lineSeparator());

        stringBuilder.append("\tdef __init__(self, ").append(getAttributeNames()).append("):");
        stringBuilder.append(System.lineSeparator());
        for (ARFFAttribute attribute : arffAttributes) {
            stringBuilder.append("\t\tself.").append(attribute.getname()).append(" = ").append(attribute.getname());
            stringBuilder.append(System.lineSeparator());
        }
        stringBuilder.append(System.lineSeparator());

        stringBuilder.append("\tdef getFunctions(self):");
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append("\t\treturn [").append(getFunctions()).append("];");
        stringBuilder.append(System.lineSeparator());
        stringBuilder.append(System.lineSeparator());

        for (String ddLabel : foundLabels) {
            stringBuilder.append("\tdef ").append(ddLabel).append("(self):");
            stringBuilder.append(System.lineSeparator());
            Predicate predicate = predNaming.predicate(ddLabel);
            String argument = predicate.argument();
            if (predicate instanceof NominalPredicate) {
                String referenceValue = ((NominalPredicate) predicate).referenceValue();
                stringBuilder.append("\t\treturn self.")
                             .append(argument)
                             .append(" == \"")
                             .append(referenceValue)
                             .append("\";");
            } else if (predicate instanceof RealPredicate) {
                double threshold = ((RealPredicate) predicate).threshold();
                stringBuilder.append("\t\treturn self.").append(argument).append(" < ").append(threshold).append(";");
            }
            stringBuilder.append(System.lineSeparator());
            stringBuilder.append(System.lineSeparator());
        }

        return stringBuilder.toString();
    }

    private String getFunctions() {
        List<String> functions = new ArrayList<>();
        for (String ddLabel : orderedLabels) {
            functions.add("lambda:self." + ddLabel + "()");
        }
        return String.join(", ", functions);
    }

    private String getAttributeNames() {
        Set<String> names = new HashSet<>();
        for (ARFFAttribute attribute : arffAttributes) {
            names.add(attribute.getname());
        }
        return String.join(", ", names);
    }

}
