package info.scce.dime.app.forest;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.enterprise.context.spi.CreationalContext;
import javax.enterprise.inject.spi.Bean;
import javax.enterprise.inject.spi.BeanManager;
import javax.enterprise.inject.spi.CDI;

import com.google.common.collect.BiMap;
import de.ls5.dywa.generated.controller.dime__HYPHEN_MINUS__models.app.ARFFAttributeController;
import de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.app.ARFFAttribute;
import de.ls5.dywa.generated.entity.dime__HYPHEN_MINUS__models.app.ARFFDatatype;
import info.scce.addlib.dd.DDManager;
import info.scce.addlib.dd.DDReorderingType;
import info.scce.addlib.dd.xdd.XDD;
import info.scce.addlib.dd.xdd.XDDManager;
import info.scce.addlib.dd.xdd.grouplikedd.MonoidDDManager;
import info.scce.addlib.infrandforest.assignment.NominalPredicate;
import info.scce.addlib.infrandforest.assignment.Predicate;
import info.scce.addlib.infrandforest.assignment.RealPredicate;
import info.scce.addlib.infrandforest.dd.DecisionDiagramTransform;
import info.scce.addlib.infrandforest.dd.PredicateNaming;
import info.scce.addlib.infrandforest.dd.labelmultiset.LabelMultiset;
import info.scce.addlib.infrandforest.dd.labelmultiset.LabelMultisetDDManager;
import info.scce.addlib.infrandforest.dd.labelvector.LabelVector;
import info.scce.addlib.infrandforest.dd.labelvector.LabelVectorDDManager;
import info.scce.addlib.infrandforest.dd.labelvector.RandomForestAdapter;
import info.scce.addlib.infrandforest.dt.DecisionTree;
import info.scce.addlib.serializer.XDDSerializer;
import info.scce.dime.app.forest.generator.DotGenerator;
import weka.classifiers.trees.RandomForest;
import weka.core.Attribute;
import weka.core.Instances;
import weka.core.converters.ArffLoader;

public class RandomForestAPI {

    private final InputStream dataSet;
    private int seed;
    private int baggingSize;
    private int numTrees;
    private boolean filterUnsatPaths;
    private boolean stepwiseAggregation;
    private BiMap<Integer, String> labelIdx;
    private PredicateNaming predNaming;
    private List<String> classLabels;
    private List<ARFFAttribute> arffAttributes;
    private int numberOfNodes;
    private int maximumDepth;
    private int pathLength;
    private boolean optimizePredicateOrder;
    private static ARFFAttributeController arffAttributesController;

    private String serializedDD;

    public RandomForestAPI(int seed,
                           int baggingSize,
                           int numTrees,
                           boolean filterUnsatPaths,
                           boolean stepwiseAggregation,
                           InputStream dataSet,
                           boolean optimizePredicateOrder) {
        this.seed = seed;
        this.baggingSize = baggingSize;
        this.numTrees = numTrees;
        this.dataSet = dataSet;
        this.filterUnsatPaths = filterUnsatPaths;
        this.stepwiseAggregation = stepwiseAggregation;
        this.optimizePredicateOrder = optimizePredicateOrder;
    }

    public String generateDecisionTrees(List<String> featureValues) throws IOException {
        MonoidDDManager<LabelVector> ddManager = new LabelVectorDDManager();
        List<XDD<LabelVector>> dds = getDTs(ddManager);
        if (optimizePredicateOrder) {
            optimizePredicateOrder(ddManager);
        }
        setStats(dds);
        List<String> rootNames = getRootNames(dds);
        DotGenerator<LabelVector> dotGenerator =
                new DotGenerator<>(dds, predNaming, new LabelVectorPrettifier(), labelIdx, rootNames);
        String generatedDot = getGeneratedDot(featureValues, dotGenerator);
        pathLength = dotGenerator.getPathLength();
        ddManager.quit();
        return generatedDot;
    }

    private List<String> getRootNames(List<XDD<LabelVector>> dds) {
        List<String> rootNames = new ArrayList<>(dds.size());
        for (int i = 0; i < dds.size(); i++) {
            rootNames.add("Tree" + (i + 1));
        }
        return rootNames;
    }

    public List<XDD<LabelVector>> getDTs(MonoidDDManager<LabelVector> ddManager) throws IOException {
        RandomForestAdapter adapter = new RandomForestAdapter(ddManager);
        RandomForest forest = adapter.getClassifier();
        buildRandomForest(forest);
        List<DecisionTree> trees = adapter.getDecisionTreeAdapter().createDecisionTrees();
        List<XDD<LabelVector>> dts = new ArrayList<>();
        for (DecisionTree tree : trees) {
            XDD<LabelVector> treeAsXDD = adapter.createDecisionDiagram(tree);
            labelIdx = adapter.labelIdx();
            predNaming = adapter.predNaming();
            if (filterUnsatPaths) {
                treeAsXDD = filterUnsatPaths(ddManager, treeAsXDD);
            }
            //TODO: deref
            dts.add(treeAsXDD);
        }
        System.out.println("Random Forest size:" + size(trees));
        return dts;
    }

    private int size(List<DecisionTree> trees) {
        int size = 0;
        for (DecisionTree tree : trees) {
            size += size(tree);
        }
        return size;
    }

    private int size(DecisionTree tree) {
        if (tree.isLeaf()) {
            return 1;
        } else {
            return 1 + size(tree.t()) + size(tree.e());
        }
    }

    private void buildRandomForest(RandomForest forest) throws IOException {
        forest.setSeed(seed);
        forest.setBagSizePercent(baggingSize);
        forest.setNumIterations(numTrees);
        Instances instances = loadArff(dataSet);
        try {
            forest.buildClassifier(instances);
        } catch (Exception e) {
            e.printStackTrace();
            throw new IllegalArgumentException("Random Forest cannot be learned from given data set.");
        }
    }

    public <E> XDD<E> filterUnsatPaths(XDDManager<E> ddManager, XDD<E> dd) {
        RedundantPredicateEliminationNew<E> elimination = new RedundantPredicateEliminationNew<>(ddManager, predNaming);
        return elimination.apply(dd);
    }

    protected Instances loadArff(InputStream dataSet) throws IOException {
        ArffLoader arffLoader = new ArffLoader();
        arffLoader.setSource(dataSet);
        Instances instances = arffLoader.getDataSet();
        if (instances.classIndex() == -1) {
            instances.setClassIndex(instances.numAttributes() - 1);
        }
        setAttributes(instances);
        setClassLabels(instances);
        return instances;
    }

    public boolean isFilterUnsatPaths() {
        return filterUnsatPaths;
    }
    
    public boolean isStepwiseAggregation() {
        return stepwiseAggregation;
    }

    public List<ARFFAttribute> getArffAttributes() {
        return arffAttributes;
    }

    private void setAttributes(Instances instances) {
        arffAttributes = new ArrayList<>();
        for (int i = 0; i < instances.numAttributes(); i++) {
            if (i != instances.classIndex()) {
                ARFFAttribute attribute = getArffAttributesController().createTransient("arffAttribute");
                attribute.setname(instances.attribute(i).name());
                attribute.setarffDataype(getDatatype(instances.attribute(i)));
                arffAttributes.add(attribute);
            }
        }
    }

    private ARFFDatatype getDatatype(Attribute attribute) {
        if (attribute.isNumeric()) {
            return ARFFDatatype.NUMERIC;
        }
        return ARFFDatatype.NOMINAL;
    }

    private static ARFFAttributeController getArffAttributesController() {
        if (arffAttributesController == null) {
            BeanManager bm = CDI.current().getBeanManager();
            Set<Bean<?>> beans = bm.getBeans(ARFFAttributeController.class);
            Bean<ARFFAttributeController> bean = (Bean<ARFFAttributeController>) bm.resolve(beans);
            CreationalContext<ARFFAttributeController> cctx = bm.createCreationalContext(bean);
            arffAttributesController =
                    (ARFFAttributeController) bm.getReference(bean, ARFFAttributeController.class, cctx);
        }
        return arffAttributesController;
    }

    public List<String> getClassLabels() {
        return classLabels;
    }

    private void setClassLabels(Instances instances) {
        classLabels = new ArrayList<>();
        int numClasses = instances.numClasses();
        for (int i = 0; i < numClasses; i++) {
            String classLabel = instances.classAttribute().value(i);
            classLabels.add(classLabel);
        }
    }

    public XDD<LabelMultiset> getVectorADD(MonoidDDManager<LabelMultiset> ddManager, boolean optimizePredicateOrder)
            throws IOException {
        info.scce.addlib.infrandforest.dd.labelmultiset.RandomForestAdapter adapter =
                new info.scce.addlib.infrandforest.dd.labelmultiset.RandomForestAdapter(ddManager);
        labelIdx = adapter.labelIdx();
        predNaming = adapter.predNaming();
        if (filterUnsatPaths) {
            DecisionDiagramTransform<LabelMultiset> transform =
                    new DecisionDiagramTransform<LabelMultiset>(ddManager, predNaming) {

                        @Override
                        public XDD<LabelMultiset> apply(XDD<LabelMultiset> xdd) {
                            return filterUnsatPaths(ddManager, xdd);
                        }
                    };
            if (stepwiseAggregation) {
            	adapter.withPostIterationTransform(transform);
            } else {
            	adapter.withPostTransform(transform);
            }          
        }
        RandomForest forest = adapter.getClassifier();
        buildRandomForest(forest);
        XDD<LabelMultiset> dd = adapter.createDecisionDiagram();
        if (optimizePredicateOrder) {
            optimizePredicateOrder(ddManager);
        }
        return dd;
    }

    public XDD<Integer> getMajorityVoteDD(XDDManager<Integer> xddManager) throws IOException {
        MonoidDDManager<LabelMultiset> ddManager = new LabelMultisetDDManager();
        XDD<LabelMultiset> vectorADD = getVectorADD(ddManager, false);
        System.out.println("vectorADD size: " + vectorADD.dagSize());
        copyVariableOrder(ddManager, xddManager);
        XDD<Integer> majorityVoteDD = vectorADD.monadicTransform(xddManager, LabelMultiset::mostFrequentLabel);
        if (optimizePredicateOrder) {
            optimizePredicateOrder(xddManager);
            if (filterUnsatPaths) {
                majorityVoteDD = filterUnsatPaths(xddManager, majorityVoteDD);
            }
        }
        vectorADD.recursiveDeref();
        ddManager.quit();
        return majorityVoteDD;
    }

    public String generateVectorDD(List<String> featureValues) throws IOException {
        MonoidDDManager<LabelMultiset> ddManager = new LabelMultisetDDManager();
        XDD<LabelMultiset> dd = getVectorADD(ddManager, this.optimizePredicateOrder);
        setStats(dd);
        DotGenerator<LabelMultiset> dotGenerator =
                new DotGenerator<>(dd, predNaming, new MultisetPrettifier(), labelIdx, "Vector ADD");
        String generatedDot = getGeneratedDot(featureValues, dotGenerator);
        pathLength = dotGenerator.getPathLength();

        dd.recursiveDeref();
        ddManager.quit();
        return generatedDot;
    }

    public String generateMajorityVoteDD(List<String> featureValues) throws IOException {
        XDDManager<Integer> xddManager = new XDDManager<>();
        XDD<Integer> majorityVoteDD = getMajorityVoteDD(xddManager);
        setStats(majorityVoteDD);
        DotGenerator<Integer> dotGenerator =
                new DotGenerator<>(majorityVoteDD, predNaming, new IntegerPrettifier(), labelIdx, "Majority Vote");
        String generatedDot = getGeneratedDot(featureValues, dotGenerator);
        pathLength = dotGenerator.getPathLength();

        XDDSerializer<Integer> serializer = new XDDSerializer<>();
        serializedDD = serializer.serialize(majorityVoteDD);

        majorityVoteDD.recursiveDeref();
        xddManager.quit();
        return generatedDot;
    }

    private <T> String getGeneratedDot(List<String> featureValues, DotGenerator<T> dotGenerator) {
        return featureValues.isEmpty() ?
                dotGenerator.generateDot() :
                dotGenerator.showPathDot(arffAttributes, featureValues);
    }

    public XDD<Integer> getClassCharacterizationDD(XDDManager<Integer> xddManager, String classLabel)
            throws IOException {
        XDD<Integer> majorityVoteDD = getMajorityVoteDD(xddManager);
        return getClassCharacterizationDD(xddManager, classLabel, majorityVoteDD);
    }

    public XDD<Integer> getClassCharacterizationDD(XDDManager<Integer> xddManager,
                                                   String classLabel,
                                                   XDD<Integer> majorityVoteDD) {
        XDD<Integer> classDD = getClassCharacterizationDD(classLabel, majorityVoteDD);
        if (optimizePredicateOrder) {
            optimizePredicateOrder(xddManager);
            if (filterUnsatPaths) {
                classDD = filterUnsatPaths(xddManager, classDD);
            }
        }
        setStats(classDD);
        return classDD;
    }

    private XDD<Integer> getClassCharacterizationDD(String classLabel, XDD<Integer> majorityVoteDD) {
        int classLabelIdx = labelIdx.inverse().get(classLabel);
        XDD<Integer> classDD = majorityVoteDD.monadicApply(integer -> {
            if (integer != classLabelIdx) {
                return -1;
            }
            return integer;
        });
        majorityVoteDD.recursiveDeref();
        return classDD;
    }

    public String generateClassCharacterization(String classLabel, List<String> featureValues) throws IOException {
        XDDManager<Integer> xddManager = new XDDManager<>();
        XDD<Integer> classDD = getClassCharacterizationDD(xddManager, classLabel);
        return serializeClassCharacterization(classLabel, xddManager, classDD, featureValues);
    }

    public String serializeClassCharacterization(String classLabel,
                                                 XDDManager<Integer> xddManager,
                                                 XDD<Integer> classDD,
                                                 List<String> featureValues) {
        labelIdx.put(-1, "NOT " + classLabel);
        DotGenerator<Integer> dotGenerator =
                new DotGenerator<>(classDD, predNaming, new IntegerPrettifier(), labelIdx, classLabel);
        dotGenerator.setClassLabel(classLabel);
        String generatedDot = getGeneratedDot(featureValues, dotGenerator);
        pathLength = dotGenerator.getPathLength();
        classDD.recursiveDeref();
        xddManager.quit();
        return generatedDot;
    }

    public String getPredictedClass(XDD<Integer> majorityVoteDD,
                                    List<ARFFAttribute> arffAttributes,
                                    List<String> featureValues) {
        Map<String, Double> numericAttributeValues = new HashMap<>();
        Map<String, String> nominalAttributeValues = new HashMap<>();
        for (int i = 0; i < arffAttributes.size(); i++) {
            String attributeName = arffAttributes.get(i).getname();
            if (arffAttributes.get(i).getarffDataype().equals(ARFFDatatype.NUMERIC)) {
                numericAttributeValues.put(attributeName, Double.parseDouble(featureValues.get(i)));
            } else {
                nominalAttributeValues.put(attributeName, featureValues.get(i));
            }
        }
        while (!majorityVoteDD.isConstant()) {
            String ddLabel = majorityVoteDD.readName();
            Predicate predicate = predNaming.predicate(ddLabel);
            if (predicate instanceof RealPredicate) {
                RealPredicate realPredicate = (RealPredicate) predicate;
                String argument = realPredicate.argument();
                double threshold = realPredicate.threshold();
                // argument < threshold
                if (numericAttributeValues.get(argument) < threshold) {
                    majorityVoteDD = majorityVoteDD.t();
                } else {
                    majorityVoteDD = majorityVoteDD.e();
                }
            } else {
                NominalPredicate nominalPredicate = (NominalPredicate) predicate;
                String argument = nominalPredicate.argument();
                String referenceValue = nominalPredicate.referenceValue();
                if (nominalAttributeValues.get(argument).equals(referenceValue)) {
                    majorityVoteDD = majorityVoteDD.t();
                } else {
                    majorityVoteDD = majorityVoteDD.e();
                }
            }
        }
        return labelIdx.get(majorityVoteDD.v());
    }

    public <T> void setStats(XDD<T> dd) {
        setStats(Collections.singletonList(dd));
    }

    public <T> void setStats(List<XDD<T>> dds) {
        setNumberOfNodes(dds);
        setMaximumDepth(dds);
    }

    public int getNumberOfNodes() {
        return numberOfNodes;
    }

    private <T> void setNumberOfNodes(List<XDD<T>> dds) {
        Set<XDD<T>> seen = new HashSet<>();
        for (XDD<T> dd : dds) {
            setNumberOfNodes(dd, seen);
        }
        this.numberOfNodes = seen.size();
    }

    public int getMaximumDepth() {
        return maximumDepth;
    }

    private <T> void setMaximumDepth(List<XDD<T>> dds) {
        Map<XDD<T>, Integer> ddToMaximumDepth = new HashMap<>();
        for (XDD<T> dd : dds) {
            this.maximumDepth = Math.max(this.maximumDepth, getMaximumDepth(dd, ddToMaximumDepth));
        }
    }

    private <T> int getMaximumDepth(XDD<T> dd, Map<XDD<T>, Integer> ddToMaximumDepth) {
        Integer maxDepth = ddToMaximumDepth.get(dd);
        if (maxDepth == null) {
            if (dd.isConstant()) {
                maxDepth = 0;
            } else {
                int trueDDMaximumDepth = getMaximumDepth(dd.t(), ddToMaximumDepth);
                int falseDDMaximumDepth = getMaximumDepth(dd.e(), ddToMaximumDepth);
                maxDepth = 1 + Math.max(trueDDMaximumDepth, falseDDMaximumDepth);
            }
            ddToMaximumDepth.put(dd, maxDepth);
        }
        return maxDepth;
    }

    private <T> void setNumberOfNodes(XDD<T> dd, Set<XDD<T>> seen) {
        if (!seen.contains(dd)) {
            seen.add(dd);
            if (!dd.isConstant()) {
                setNumberOfNodes(dd.t(), seen);
                setNumberOfNodes(dd.e(), seen);
            }
        }
    }

    private void copyVariableOrder(XDDManager<?> fromDDManager, XDDManager<?> toDDManager) {
        List<Map.Entry<String, Integer>> varNameIdxList = new ArrayList<>(fromDDManager.knownVarNames().entrySet());
        varNameIdxList.sort(Comparator.comparingInt(Map.Entry::getValue));
        for (Map.Entry<String, Integer> varNameIdx : varNameIdxList) {
            toDDManager.varIdx(varNameIdx.getKey());
        }
    }

    private void optimizePredicateOrder(DDManager<?, ?> ddManager) {
        ddManager.reduceHeap(DDReorderingType.GROUP_SIFT_CONV, 0);
    }

    public PredicateNaming getPredNaming() {
        return predNaming;
    }

    public BiMap<Integer, String> getLabelIdx() {
        return labelIdx;
    }

    public int getPathLength() {
        return pathLength;
    }

    public void setPathLength(int pathLength) {
        this.pathLength = pathLength;
    }

    public String getSerializedDD() {
        return serializedDD;
    }

}
